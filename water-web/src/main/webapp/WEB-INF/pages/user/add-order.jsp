<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Order | Water Delivery</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>

<%--message--%>
<c:if test="${not empty message}">
    <div class="error">
        <c:out value="${message}"/>
        <c:remove var="message" scope="session"/>
    </div>
</c:if>

<div class="header">
    Информация для доставки
</div>
<div class="form">
    <form:form action="${pageContext.request.contextPath}/user/order/add" method="post" modelAttribute="orderSubmitDTO">
        <div class="required-warning">
            <span style="font-size: 1.5em; color: red"> *</span> Все поля обязательны для заполнения.
        </div>
        <div>
            <div class="form-column">

            </div>
        </div>
        <div>
            <div class="form-column">
                Адрес:<br>
                <form:radiobutton id="01" class="radio-hide" name="addressInput" value="choose" checked="checked"
                                  path="addressInput"/>
                <label for="01">Выбрать из списка:<br></label>
                <select name="selectedAddressId">
                    <c:forEach var="address" items="${userFull.addresses}">
                        <option value="${address.id}"
                                <c:if test="${sessionScope.oldOrderDTO.deliveryAddressDTO.id eq address.id}">
                                    <c:out value="selected"/>
                                </c:if>
                        ><c:out value="г. ${address.city}, ${address.address}"/></option>
                    </c:forEach>
                </select>
                <form:radiobutton id="02" class="radio-hide" name="addressInput" value="new" path="addressInput"/>
                <label for="02">Ввести новый:<br></label>
                <div class="form-column">Выберите город:
                    <div class="error">
                        <form:errors path="city"/><br>
                    </div>
                    <form:select name="city" path="city">
                        <spring:message code="label.city" var="city"/>
                        <spring:message code="label.minsk" var="minsk"/>
                        <spring:message code="label.grodno" var="grodno"/>
                        <spring:message code="label.gomel" var="gomel"/>
                        <spring:message code="label.brest" var="brest"/>
                        <form:option selected="true" disabled="disabled" value="" label="${city}"/>
                        <form:option value="Минск" label="${minsk}"/>
                        <form:option value="Гродно" label="${grodno}"/>
                        <form:option value="Гомель" label="${gomel}"/>
                        <form:option value="Брест" label="${brest}"/>
                    </form:select> <br/>
                    Адрес доставки: <br/>
                    <div class="error">
                        <form:errors path="address"/>
                    </div>
                    <form:textarea name="address" rows="5"
                                   cols="30" placeholder="Введите адрес" path="address"/><br/>
                </div>
                <br>Телефон:<br>
                <form:radiobutton id="03" class="radio-hide" name="phoneInput" value="choose"
                                  checked="checked" path="phoneInput"/>
                <label for="03">Выбрать из списка:<br></label>
                <select name="selectPhone">
                    <c:forEach var="phone" items="${userFull.phones}">
                        <option value="${phone.id}"
                        <c:if test="${sessionScope.oldOrderDTO.deliveryPhoneDTO.id eq phone.id}">
                            <c:out value="selected"/>
                        </c:if>
                        ><c:out value="${phone.phone}"/></option>
                    </c:forEach>
                </select>
                <form:radiobutton id="04" class="radio-hide" name="phoneInput" value="new"
                                  path="phoneInput"/>
                <label for="04">Ввести новый:<br></label>
                <div class="form-column">
                    <div class="error">
                        <form:errors path="phone"/>
                    </div>
                    +375 <form:input name="phone" class="phone" type="text" pattern="[0-9]{9}" maxlength="9"
                                     placeholder="Телефон (9 цифр)" path="phone"/>
                </div>
            </div>
            <div class="form-column">
                <fmt:formatDate value="${sessionScope.oldOrderDTO.deliveryDate}" pattern="yyyy-MM-dd" var="oldDate"/>
                    <%--<c:out value="${oldDate}"/>--%>
                <label>Дата доставки <form:input name="date" type="date" value="${oldDate}"
                                                 required="true" path="date"/></label>
                <label>Время доставки: <form:select name="time" path="time" required="true">
                    <form:option value="" label="выбрать"/>
                    <c:choose>
                        <c:when test="${not empty oldOrderDTO.deliveryTimeDTO}">
                            <option <c:if test="${oldOrderDTO.deliveryTimeDTO eq 'MORNING'}">
                                <c:out value="selected"/></c:if>>9:00 - 13:00
                            </option>
                            <option <c:if test="${oldOrderDTO.deliveryTimeDTO eq 'MIDDAY'}">
                                <c:out value="selected"/></c:if>>13:00 - 17:00
                            </option>
                            <option <c:if test="${oldOrderDTO.deliveryTimeDTO eq 'EVENING'}">
                                <c:out value="selected"/></c:if>>17:00 - 21:00
                            </option>
                        </c:when>
                        <c:otherwise>
                            <form:option value="9:00 - 13:00"/>
                            <form:option value="13:00 - 17:00"/>
                            <form:option value="17:00 - 21:00"/>
                        </c:otherwise>
                    </c:choose>
                </form:select> </label><br/>
                <label>Дополнительная информация: <br/>
                    <c:choose>
                        <c:when test="${not empty oldOrderDTO.deliveryInformation}">
                            <textarea name="info" rows="5" cols="30" placeholder="Дополнительная информация"><c:out
                                    value="${oldOrderDTO.deliveryInformation}"/></textarea>
                        </c:when>
                        <c:otherwise>
                            <form:textarea name="info" rows="5" cols="30" value="${oldOrderDTO.deliveryInformation}"
                                           path="info" placeholder="Дополнительная информация"/>
                        </c:otherwise>
                    </c:choose>
                </label><br/>
                <input class="hidden" name="oldOrderId" value="${sessionScope.oldOrderDTO.id}">
            </div>
        </div>
        <input type="submit" value="
<c:choose>
<c:when test="${not empty oldOrderDTO}">
<c:out value="Обновить заказ"/>
</c:when>
<c:otherwise>
<c:out value="Оформить заказ"/>
</c:otherwise>
</c:choose>
">
    </form:form>
</div>
</body>
</html>
