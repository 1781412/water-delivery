<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Order | Water Delivery</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<h1><c:out value="Order # ${orderDTO.id}"/></h1>
<%--<input form="delete" name="id" type="number" value="${userDTO.id}" class="hidden">--%>

<%--message--%>
<c:if test="${not empty message}">
    <div class="error">
        <c:out value="${message}"/>
        <c:remove var="message" scope="session"/>
    </div>
</c:if>


<div class="table">
    <table>
        <tr>
            <th align="left">
                <c:if test="${orderDTO.statusDTO eq 'NEW'}">
                <form action="${pageContext.request.contextPath}/user/order/edit" method="post">
                    <button class="green-button" type="submit" name="orderId" value="${orderDTO.id}">Изменить</button>
                </form>

                <form action="${pageContext.request.contextPath}/user/order/delete" method="post">
                    <button class="red-button" type="submit" name="orderId" value="${orderDTO.id}">Удалить</button>
                </form>
            </th>
            </c:if>
        </tr>
        <tr>
            <td>№:</td>
            <td><c:out value="${orderDTO.id}"/></td>
        </tr>
        <tr>
            <td>Создан:</td>
            <td><fmt:formatDate value="${orderDTO.creationDate}" pattern="dd.MM.yyyy HH:mm"/></td>
        </tr>
        <tr>
            <td>Дата и время доставки:</td>
            <td><fmt:formatDate value="${orderDTO.deliveryDate}" pattern="dd MMM"/>,
                <c:choose>
                    <c:when test="${orderDTO.deliveryTimeDTO eq 'MORNING'}">
                        <c:out value="9:00 - 13:00"/>
                    </c:when>
                    <c:when test="${orderDTO.deliveryTimeDTO eq 'MIDDAY'}">
                        <c:out value="13:00 - 17:00"/>
                    </c:when>
                    <c:when test="${orderDTO.deliveryTimeDTO eq 'EVENING'}">
                        <c:out value="17:00 - 21:00"/>
                    </c:when>
                </c:choose>
            </td>
        </tr>
        <tr>
            <td>Status:</td>
            <td class="user-property">
                <c:out value="${orderDTO.statusDTO}"/>

            </td>
        </tr>
        <tr>
            <td>Address:</td>
            <td><c:out value="${orderDTO.deliveryAddressDTO.city}, ${orderDTO.deliveryAddressDTO.address}"/></td>
        </tr>
        <tr>
            <td>Phone:</td>
            <td><c:out value="${orderDTO.deliveryPhoneDTO.phone}"/></td>
        </tr>
        <tr>
            <td>Additional information:</td>
            <td><c:out value="${orderDTO.deliveryInformation}"/></td>
        </tr>
        <tr>
            <td>Order:</td>
            <td>
                <table class="margin-off">
                    <c:forEach var="item" items="${orderDTO.basketDTO}">
                        <tr>
                            <td><c:out value="${item.productDTO.name};"/></td>
                            <td><c:out value="${item.productDTO.price} р."/></td>
                            <td><c:out value="${item.quantity} шт."/></td>
                        </tr>
                    </c:forEach>
                </table>
            </td>
        </tr>
        <tr>
            <td>Total cost:</td>
            <td><b><c:out value="${orderDTO.cost}"/></b></td>
        </tr>
    </table>
</div>
</body>
</html>