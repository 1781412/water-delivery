<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
    <title>Заказы | Water delivery</title>
</head>
<body>
<h1>Заказы</h1>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<div class="table">

    <%--message--%>
    <c:if test="${not empty message}">
        <div class="error">
            <c:out value="${message}"/>
            <c:remove var="message" scope="session"/>
        </div>
    </c:if>

    <table width="900px">
        <tr>
            <th>№</th>
            <th>Дата доставки</th>
            <th>Время доставки</th>
            <th width="130px">Адрес</th>
            <th width="340px">Наименование | Цена | Количество</th>
            <th>Стоимость</th>
            <th>Статус</th>
        </tr>
        <c:forEach var="order" items="${requestScope.ordersList}">
            <tr class="table-row">
                <td><a href="${pageContext.request.contextPath}/user/order/show/${order.id}">
                    <c:out value="${order.id}"/></a></td>
                <td><fmt:formatDate value="${order.deliveryDate}" pattern="dd MMM"/></td>
                <td>
                    <c:choose>
                        <c:when test="${order.deliveryTimeDTO eq 'MORNING'}">
                            <c:out value="9:00 - 13:00"/>
                        </c:when>
                        <c:when test="${order.deliveryTimeDTO eq 'MIDDAY'}">
                            <c:out value="13:00 - 17:00"/>
                        </c:when>
                        <c:when test="${order.deliveryTimeDTO eq 'EVENING'}">
                            <c:out value="17:00 - 21:00"/>
                        </c:when>
                    </c:choose>
                </td>
                <td><c:out value="${order.deliveryAddressDTO.address}"/></td>
                <td>
                    <table class="margin-off">
                        <c:forEach var="item" items="${order.basketDTO}">
                            <tr>
                                <td width="130px"><c:out value="${item.productDTO.name}"/></td>
                                <td style="width: 70px"><c:out value="${item.productDTO.price} р."/></td>
                                <td style="width: 70px"><c:out value="${item.quantity} шт."/></td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
                <td><b><c:out value="${order.cost} р."/></b></td>

                <c:choose>
                    <c:when test="${order.statusDTO eq 'NEW'}">
                        <td>
                            <form action="${pageContext.request.contextPath}/user/order/edit" method="post">
                                <button class="green-button" type="submit" name="orderId" value="${order.id}">Изменить</button>
                            </form>
                            <form action="${pageContext.request.contextPath}/user/order/delete" method="post">
                                <button class="red-button" type="submit" name="orderId" value="${order.id}">Удалить</button>
                            </form>
                        </td>
                    </c:when>
                    <c:when test="${order.statusDTO eq 'IN_PROGRESS'}">
                        <td colspan="2"> Отправлен</td>
                    </c:when>
                    <c:when test="${order.statusDTO eq 'DELIVERED'}">
                        <td colspan="2"> Доставлен</td>
                    </c:when>
                    <c:otherwise>
                        <td colspan="2"> В работе</td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </c:forEach>
    </table>
    <%--    <form id="delete" action="${pageContext.request.contextPath}/admin/products/delete" method="post">
            <input type="submit" class="register" value="Удалить">
        </form>--%>
</div>

<div class="pagination">
    <c:choose>
        <c:when test="${not empty param.page}">
            <c:set value="${param.page}" var="currentPage"/>
        </c:when>
        <c:otherwise>
            <c:set value="1" var="currentPage"/>
        </c:otherwise>
    </c:choose>
    <a href="?page=${currentPage - 1}">&laquo;</a>
    <c:forEach var="pageNum" begin="1" end="${requestScope.pagesQuantity}">
        <c:choose>
            <c:when test="${currentPage eq pageNum}">
                <a class="active" href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:when>
            <c:otherwise>
                <a href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <a href="?page=${currentPage + 1}">&raquo;</a>
</div>

</body>
</html>