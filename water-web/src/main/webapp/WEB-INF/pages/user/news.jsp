<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>News | Water Delivery </title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<h1>News</h1>
<c:forEach var="post" items="${requestScope.newsList}">
    <article>
        <div class="news-headline">
            <div class="news-date"><fmt:formatDate value="${post.publicationDate}" pattern="dd-MM-yyyy HH:mm"/></div>
            <h3><c:out value="${post.headline}"/></h3>
        </div>
        <div class="news-img">
            <img src="${pageContext.request.contextPath}/download/${post.imageId}"/>
        </div>
        <div class="news-text"><p><c:out value="${post.newsText}" escapeXml="t"/></p>
        </div>
    </article>
</c:forEach>

<div class="pagination">
    <c:choose>
        <c:when test="${not empty param.page}">
            <c:set value="${param.page}" var="currentPage"/>
        </c:when>
        <c:otherwise>
            <c:set value="1" var="currentPage"/>
        </c:otherwise>
    </c:choose>
    <a href="?page=${currentPage - 1}">&laquo;</a>
    <c:forEach var="pageNum" begin="1" end="${requestScope.pagesQuantity}">
        <c:choose>
            <c:when test="${currentPage eq pageNum}">
                <a class="active" href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:when>
            <c:otherwise>
                <a href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <a href="?page=${currentPage + 1}">&raquo;</a>
</div>
</body>
</html>