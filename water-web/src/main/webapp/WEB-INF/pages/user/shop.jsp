<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Water Delivery </title>
    <link href="${pageContext.request.contextPath}/resources/css/shop.css" rel="stylesheet">
</head>
<body>

<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>

<div class="central-column">

    <%--message--%>
    <c:if test="${not empty message}">
        <div class="error">
            <c:out value="${message}" escapeXml="false"/>
            <c:remove var="message" scope="session"/>
        </div>
    </c:if>


    <c:forEach var="product" items="${requestScope.productsList}">
        <div class="product">
            <div class="name"><c:out value="${product.name}"/></div>
            <br>
            <div class="image">
                <img src="${pageContext.request.contextPath}/download/${product.imageId}"/>
            </div>
            <form action="${pageContext.request.contextPath}/user/shop" method="post">
                <div class="price"><c:out value="${product.price}"/> р</div>
                <br>
                <input class="hidden" name="page" type="number" value="${param.page}">
                <input class="hidden" name="productId" type="text" value="${product.id}">
                <input name="itemsQuantity" type="number" value="1" min="1" max="100">
                шт.
                <input type="submit" value="в корзину">
            </form>
        </div>
    </c:forEach>


    <div class="pagination">
        <c:choose>
            <c:when test="${not empty param.page}">
                <c:set value="${param.page}" var="currentPage"/>
            </c:when>
            <c:otherwise>
                <c:set value="1" var="currentPage"/>
            </c:otherwise>
        </c:choose>
        <a href="?page=${currentPage - 1}">&laquo;</a>
        <c:forEach var="pageNum" begin="1" end="${requestScope.pagesQuantity}">
            <c:choose>
                <c:when test="${currentPage eq pageNum}">
                    <a class="active" href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
                </c:when>
                <c:otherwise>
                    <a href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <a href="?page=${currentPage + 1}">&raquo;</a>
    </div>

</div>


</body>
</html>
