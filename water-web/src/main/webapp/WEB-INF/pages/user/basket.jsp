<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Water Delivery </title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>

<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<h1>Корзина</h1>
<div class="table">

    <%--message--%>
    <c:if test="${not empty message}">
        <div class="error">
            <c:out value="${message}"/>
            <c:remove var="message" scope="session"/>
        </div>
    </c:if>

    <form id="add" action="${pageContext.request.contextPath}/user/shop" method="get">
        <input type="submit" class="register" value="Добавить">
    </form>

    <c:choose>
        <c:when test="${not empty requestScope.basket}">
            <table>
                <tr>
                    <th>Наименоване</th>
                    <th>Цена</th>
                    <th>Кол-во</th>
                    <th>Стоимость</th>
                    <th></th>
                </tr>
                <c:forEach var="basketitem" items="${requestScope.basket}">
                    <tr>
                        <td><c:out value="${basketitem.productDTO.name}"/></td>
                        <td><c:out value="${basketitem.productDTO.price} р."/></td>
                        <td><c:out value="${basketitem.quantity} шт."/></td>
                        <td><c:out value="${basketitem.cost} р."/></td>
                        <td>
                            <a href="${pageContext.request.contextPath}/user/basket/delete/${basketitem.productDTO.id}">
                                <img class="icon" src="${pageContext.request.contextPath}/resources/img/news/delete.png"
                                     title="Удалить"/></a></td>
                    </tr>
                </c:forEach>
                <tr>
                    <td></td>
                    <td></td>
                    <td><b>Итого:</b></td>
                    <td><b><c:out value="${cost} р."/></b></td>
                    <td></td>
                </tr>
            </table>
            <form id="delete" action="${pageContext.request.contextPath}/user/basket/clear" method="post">
                <input type="submit" class="register" value="Очистить корзину">
            </form>
            <form id="addOrder" action="${pageContext.request.contextPath}/user/order/add" method="get">
                <input class="hidden" form="addOrder">
                <input type="submit" value="
<c:choose>
<c:when test="${not empty sessionScope.oldOrderDTO}">
<c:out value="Обновить заказ"/>
</c:when>
<c:otherwise>
<c:out value="Оформить заказ"/>
</c:otherwise>
</c:choose>
">
            </form>
        </c:when>
        <c:otherwise>
            <div class="empty-basket">
                Корзина пуста
            </div>
        </c:otherwise>
    </c:choose>
</div>
</body>
</html>