<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:message code="label.new.user" var="labelNewUser"/>
<spring:message code="label.update.user" var="labelUpdateUser"/>
<spring:message code="label.registration" var="labelRegistration"/>
<spring:message code="warning.fields.required" var="warningFieldsRequired"/>
<spring:message code="label.password" var="labelPassword"/>
<spring:message code="label.repeat.password" var="labelRepeatPassword"/>
<spring:message code="label.last.name" var="lastName"/>
<spring:message code="label.name" var="name"/>
<spring:message code="label.middle.name" var="middleName"/>
<spring:message code="label.address" var="address"/>
<spring:message code="label.phone" var="phone"/>
<spring:message code="label.additionalInfo" var="additionalInfo"/>
<spring:message code="label.city" var="city"/>
<spring:message code="label.minsk" var="minsk"/>
<spring:message code="label.grodno" var="grodno"/>
<spring:message code="label.gomel" var="gomel"/>
<spring:message code="label.brest" var="brest"/>
<spring:message code="label.register" var="register"/>
<spring:eval expression="userDTO.id==null ? labelNewUser:labelUpdateUser" var="formTittle"/>

<html>
<head>
    <title><c:out value="${formTittle}"/> | Water Delivery</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">

</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
    <button form="lang" class="lang" type="submit" name="lang" value="ru_RU">RU</button>
    <button form="lang" class="lang" type="submit" name="lang" value="en_US">EN</button>
    <form hidden id="lang"></form>
</div>

<%--error message--%>
<c:if test="${param['error']}">
<div class="error">
    <form:errors path="error"/>
</div>
</c:if>


<div class="header">
    <c:out value="${labelRegistration}"/>
</div>
<div class="form">
    <form:form action="${pageContext.request.contextPath}/new-user" method="post" modelAttribute="userDTO">

        <div class="required-warning">
            <span style="font-size: 1.5em; color: red"> *</span> <c:out value="${warningFieldsRequired}"/>
        </div>

        <div>
            <div class="form-column">
                <!--<label for="login">Login</label>-->
                <div class="error">
                    <form:errors path="login"/>
                </div>
                <form:input id="login" name="login" type="email" placeholder="E-mail" required="required"
                            value="${param.login}" path="login"/><span style="font-size: 1.5em; color: red"> *</span>
                <div class="error">
                    <form:errors path="password"/>
                </div>
                <form:input name="pass" type="password" placeholder="${labelPassword}" required="required"
                            path="password"/><span style="font-size: 1.5em; color: red"> *</span>
                <form:input name="passrepeat" type="password" placeholder="${labelRepeatPassword}" required="required"
                            path="passrepeat"/><span style="font-size: 1.5em; color: red"> *</span>
            </div>
            <div class="form-column">
                <form:input name="lastname" type="text" placeholder="${lastName}" required="required"
                            value="${param.lastname}" path="lastName"/><span
                    style="font-size: 1.5em; color: red"> *</span>
                <form:input name="firstname" type="text" placeholder="${name}" required="required"
                            value="${param.firstname}" path="name"/>
                <span style="font-size: 1.5em; color: red"> *</span>
                <form:input name="middlename" type="text" placeholder="${middleName}" required="required"
                            value="${param.middlename}" path="middleName"/><span
                    style="font-size: 1.5em; color: red"> *</span>
            </div>
        </div>
        <div>
            <div class="form-column">
                <div class="error">
                    <form:errors path="city"/>
                </div>
                <form:select name="city" path="city">
                    <c:choose>
                        <c:when test="${not empty param.city}">
                            <form:option selected="selected" value="${param.city}"/>
                        </c:when>
                        <c:when test="${empty param.city}">
                            <form:option value="" label="${city}" disabled="true" selected="selected"/>
                        </c:when>
                    </c:choose>
                    <form:option value="Минск" label="${minsk}"/>
                    <form:option value="Гродно" label="${grodno}"/>
                    <form:option value="Гомель" label="${gomel}"/>
                    <form:option value="Брест" label="${brest}"/>
                </form:select><span style="font-size: 1.5em; color: red"> *</span>
                <label for="address"><!--Адрес доставки: --></label>
                <form:textarea id="address" name="address" rows="5" cols="30" placeholder="${address}"
                               required="required" path="address" value="${param.address}"/>
                <span style="font-size: 1.5em; color: red; vertical-align: top"> *</span>

            </div>

            <div class="form-column">
                +375 <form:input name="phone" class="phone" type="text" placeholder="${phone}" maxlength="9"
                                 pattern="[0-9]{9}" required="required" value="${param.phone}" path="phone"/><span
                    style="font-size: 1.5em; color: red"> *</span>
                <label><!--Дополнительная информация: -->
                    <form:textarea name="info" rows="5" cols="30" placeholder="${additionalInfo}"
                                   path="additionalInfo" value="${param.info}"/>
                    <span style="font-size: 1.5em; color: rgba(255,0,0,0); vertical-align: top"> *</span></label>

            </div>
        </div>
        <input type="submit" value="${register}"/>
    </form:form>
</div>
</html>
