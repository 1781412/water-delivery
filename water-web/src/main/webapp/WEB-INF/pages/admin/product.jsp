<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Product | Water Delivery</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<h1><c:out value="${product.invNum}"/></h1>
<input form="copy" name="id" type="number" value="${product.id}" class="hidden">
<input form="update" name="id" type="number" value="${product.id}" class="hidden">

<%--message--%>
<c:if test="${not empty message}">
    <div class="error">
        <c:out value="${message}"/>
        <c:remove var="message" scope="session"/>
    </div>
</c:if>

<article>
    <div class="news-headline">
    </div>
    <div class="news-img">
        <img src="${pageContext.request.contextPath}/download/${product.imageId}"/>
    </div>
    <div class="news-text">
        <div class="table">
            <table class="margin-off">
                <tr>
                    <td>id:</td>
                    <td><c:out value="${product.id}"/></td>
                </tr>
                <tr>
                    <td>Инв №:</td>
                    <td><c:out value="${product.invNum}"/></td>
                </tr>
                <tr>
                    <td>Наименование:</td>
                    <td><c:out value="${product.name}"/></td>
                </tr>
                <tr>
                    <td>Описание:</td>
                    <td><c:out value="${product.description}"/></td>
                </tr>
                <tr>
                    <td>Цена:</td>
                    <td><c:out value="${product.price}"/></td>
                </tr>
                <tr>
                    <td>Status:</td>
                    <td class="user-property">
                        <form id="statusChange" name="statusChange"
                              action="${pageContext.request.contextPath}/admin/product/status-change/${product.id}"
                              method="post">
                            <c:choose>
                                <c:when test="${product.available eq true}">
                                    <c:out value="В наличии"/>
                                </c:when>
                                <c:otherwise>
                                    <c:out value="Нет в наличии"/>
                                </c:otherwise>
                            </c:choose>
                            <select name="status" class="status-change"
                                    onchange="document.forms['statusChange'].submit()">
                                <option>изменить</option>
                                <option value="true">В наличии</option>
                                <option value="false">Нет</option>
                            </select>
                        </form>
                    </td>
                </tr>
                <tr>
                    <td>Дата добавления:</td>
                    <td><fmt:formatDate value="${product.creationDate}" pattern="dd-MM-yyyy HH:mm"/></td>
                </tr>
                <tr>
                    <td>Последним изменения вносил:</td>
                    <td><c:out
                            value="${product.userDTO.name} ${product.userDTO.middleName} ${product.userDTO.lastName}"/></td>
                </tr>
            </table>
            <form id="copy" action="${pageContext.request.contextPath}/admin/product/copy/${product.id}" method="post">
                <input type="submit" class="register" value="Копировать">
            </form>
            <c:if test="${product.isUsed eq false}">
                <form id="update" action="${pageContext.request.contextPath}/admin/product/update" method="post">
                    <input type="submit" class="register" value="Обновить">
                </form>
            </c:if>
        </div>
    </div>
</article>
</body>
</html>