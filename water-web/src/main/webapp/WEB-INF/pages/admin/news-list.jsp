<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
    <title>Новости | Water delivery</title>
</head>
<body>
<h1>News</h1>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<div class="table">
    <form action="${pageContext.request.contextPath}/admin/news/add">
        <input type="submit" value="Добавить">
    </form>
    <%--message--%>
    <c:if test="${not empty message}">
        <div class="error">
            <c:out value="${message}"/>
            <c:remove var="message" scope="session"/>
        </div>
    </c:if>

    <table>
        <tr>
            <th></th>
            <th>Заголовок</th>
            <th>Автор</th>
            <th>Дата публикации</th>
        </tr>
        <c:forEach var="post" items="${requestScope.newsList}">
            <tr class="table-row">
                <td><input form="delete" class="checkbox" type="checkbox" name="id" value="${post.id}"></td>
                <td class="name-cell"><a href="${pageContext.request.contextPath}/admin/news/show/${post.id}">
                    <c:out value="${post.headline}"/></a></td>
                <td><c:out value="${post.userDTO.name} ${post.userDTO.lastName}"/></td>
                <c:choose>
                    <c:when test="${post.visible eq true}">
                        <td><fmt:formatDate value="${post.publicationDate}" pattern="dd-MM-yyyy HH:mm"/></td>
                    </c:when>
                    <c:otherwise>
                        <td><i>Черновик</i></td>
                    </c:otherwise>
                </c:choose>
            </tr>
        </c:forEach>
    </table>
    <form id="delete" action="${pageContext.request.contextPath}/admin/news/delete" method="post">
        <input type="submit" class="register" value="Удалить">
    </form>
</div>

<div class="pagination">
    <c:choose>
        <c:when test="${not empty param.page}">
            <c:set value="${param.page}" var="currentPage"/>
        </c:when>
        <c:otherwise>
            <c:set value="1" var="currentPage"/>
        </c:otherwise>
    </c:choose>
    <a href="?page=${currentPage - 1}">&laquo;</a>
    <c:forEach var="pageNum" begin="1" end="${requestScope.pagesQuantity}">
        <c:choose>
            <c:when test="${currentPage eq pageNum}">
                <a class="active" href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:when>
            <c:otherwise>
                <a href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <a href="?page=${currentPage + 1}">&raquo;</a>
</div>

</body>
</html>