<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>News | Water Delivery </title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>

<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>

<h1>News</h1>

<article>
    <div class="news-headline">
        <div class="news-date"><fmt:formatDate value="${post.publicationDate}" pattern="dd-MM-yyyy HH:mm"/></div>
        <h3><c:out value="${post.headline}"/></h3>
    </div>
    <div class="news-img">
        <%--<img src="${pageContext.request.contextPath}/image?path=${post.imagePath}"/>--%>
        <img src="${pageContext.request.contextPath}/download/${post.imageId}"/>
    </div>
    <div class="news-text"><p><c:out value="${post.newsText}" escapeXml="t"/></p>
    </div>
    <div class="news-headline">
        <a href="${pageContext.request.contextPath}/admin/news/edit/${post.id}">
            <img class="icon" src="${pageContext.request.contextPath}/resources/img/news/edit.png"
                 title="Редактировать"/></a>
        <a href="${pageContext.request.contextPath}/admin/news/delete?id=${post.id}">
            <img class="icon" src="${pageContext.request.contextPath}/resources/img/news/delete.png"
                 title="Удалить"/></a>
        <c:if test="${post.visible eq false}">
            <a href="${pageContext.request.contextPath}/admin/news/publish/${post.id}">
                <img class="icon" src="${pageContext.request.contextPath}/resources/img/news/publish.png"
                     title="Опубликовать"/></a>
        </c:if>
    </div>
</article>

</body>
</html>