<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>News | Water Delivery </title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>

<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>

<h1>News</h1>

<article>
    <div class="news-headline">
        <div class="news-date"><fmt:formatDate value="${messageDTO.date}" pattern="dd-MM-yyyy HH:mm"/></div>
        <h3><c:out value="${messageDTO.subject}"/></h3>
    </div>
    <div class="news-text"><p><c:out value="${messageDTO.message}" escapeXml="t"/></p>
    </div>
    <div class="news-headline">
        <a href="${pageContext.request.contextPath}/admin/messages/delete?id=${messageDTO.id}">
            <img class="icon" src="${pageContext.request.contextPath}/resources/img/news/delete.png"
                 title="Удалить"/></a>
    </div>
</article>

</body>
</html>