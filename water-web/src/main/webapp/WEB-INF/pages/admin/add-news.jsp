<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Добавление новости | Water Delivery</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<div class="news-add-body">
    <div class="header">
        <c:out value="News"/>
    </div>
    <%--message--%>
    <c:if test="${not empty error}">
        <div class="error">
            <c:out value="${error}"/>
            <c:remove var="error" scope="session"/>
        </div>
    </c:if>
    <div class="form">
        <form:form action="${pageContext.request.contextPath}/admin/news/add" enctype="multipart/form-data"
                   method="post" modelAttribute="post">
            <form:input cssClass="newsheadline" name="headline" type="text" placeholder="Заголовок"
                        value="${post.headline}" path="headline"/>
            <form:textarea class="newstext" name="newstext" placeholder="Текст новости" path="newsText" value="${post.newsText}"/>
            <label for="news-image">Фото для публикации: </label>
            <form:input cssClass="file" name="news-image" id="news-image" type="file" accept="image/jpeg"
                        path="file"/><br>

            <%--<label for="date">Дата и время публикации: </label>
            <c:choose>
                <c:when test="${not empty date}">
                    <c:set value="${date}" var="pubDate"/>
                </c:when>
                <c:otherwise>
                    <fmt:formatDate value="${post.publicationDate}" pattern="dd-MM-yyyy HH:mm" var="pubDate"/>
                </c:otherwise>
            </c:choose>
            <input name="publDate" id="date" type="text" pattern="[0-3][0-9]-[0-1][0-9]-[0-9]{4} [0-9]{2}:[0-9]{2}"
                   path="publicationDate"
                        value="${pubDate}"/>--%>
            <br><br>

            <form:checkbox cssClass="checkbox" name="visible" id="visible" path="visible"/><label for="visible">Опубликовать
            сейчас</label>
            <br>
            <c:choose>
                <c:when test="${not empty post.id}">
                    <c:out value="${post.id}"/>
                    <form:input path="id" cssClass="hidden" value="${post.id}"/>
                    <c:set var="post" value="${post}" scope="session"/>
                    <input type="submit" value="Обновить новость"/>
                </c:when>
                <c:otherwise>
                    <input type="submit" value="Добавить новость"/>
                </c:otherwise>
            </c:choose>
            <%--<input type="submit" value="Добавить новость"/>--%>
        </form:form>
    </div>
</div>
</body>
</html>
