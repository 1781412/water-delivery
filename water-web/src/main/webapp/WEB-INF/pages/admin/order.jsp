<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <title>Order | Water Delivery</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<h1><c:out value="Order # ${orderDTO.id}"/></h1>
<input form="statusChange" name="id" type="number" value="${orderDTO.id}" class="hidden">
<%--<input form="delete" name="id" type="number" value="${userDTO.id}" class="hidden">--%>

<%--message--%>
<c:if test="${not empty message}">
    <div class="error">
        <c:out value="${message}"/>
        <c:remove var="message" scope="session"/>
    </div>
</c:if>


<div class="table">
    <table>
        <tr>
            <td>№:</td>
            <td><c:out value="${orderDTO.id}"/></td>
        </tr>
        <tr>
            <td>Создан:</td>
            <td><fmt:formatDate value="${orderDTO.creationDate}" pattern="dd.MM.yyyy HH:mm"/></td>
        </tr>
        <tr>
            <td>Дата и время доставки:</td>
            <td><fmt:formatDate value="${orderDTO.deliveryDate}" pattern="dd MMM"/>
                <c:out value=" ${orderDTO.deliveryTimeDTO}"/></td>
        </tr>
        <tr>
            <td>Status:</td>
            <td class="user-property">
                <form id="statusChange" name="statusChange" action="/admin/order/show/${orderDTO.id}"
                      method="post"><c:out value="${orderDTO.statusDTO}"/>
                    <select name="status" class="status-change" onchange="document.forms['statusChange'].submit()">
                        <option>изменить</option>
                        <option>NEW</option>
                        <option>REVIEWING</option>
                        <option>IN_PROGRESS</option>
                        <option>DELIVERED</option>
                    </select>
                </form>
            </td>
        </tr>
        <tr>
            <td>Name:</td>
            <td><c:out value="${orderDTO.userDTO.name}"/></td>
        </tr>
        <tr>
            <td>Address:</td>
            <td><c:out value="${orderDTO.deliveryAddressDTO.city}, ${orderDTO.deliveryAddressDTO.address}"/></td>
        </tr>
        <tr>
            <td>Phone:</td>
            <td><c:out value="${orderDTO.deliveryPhoneDTO.phone}"/></td>
        </tr>
        <tr>
            <td>Additional information:</td>
            <td><c:out value="${orderDTO.deliveryInformation}"/></td>
        </tr>
        <tr>
            <td>Order:</td>
            <td>
                <table class="margin-off">
        <c:forEach var="item" items="${orderDTO.basketDTO}">
            <tr>
                <td><a href="${pageContext.request.contextPath}/admin/product/${item.productDTO.id}">
                    <c:out value="${item.productDTO.invNum}"/></a></td>
                <td><c:out value="${item.productDTO.name}"/></td>
                <td><c:out value="${item.quantity} шт."/></td>
            </tr>
        </c:forEach>
                </table>
            </td>
        </tr>
        <tr>
            <td>Total cost:</td>
            <td><b><c:out value="${orderDTO.cost}"/></b></td>
        </tr>
    </table>
    <%--<form id="delete" action="${pageContext.request.contextPath}/superadmin/users/delete" method="post">
        <input type="submit" class="register" value="Удалить">
    </form>--%>
</div>
</body>
</html>