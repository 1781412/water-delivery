<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
    <title>Заказы | Water delivery</title>
</head>
<body>
<h1>Заказы</h1>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<div class="table">

    <%--message--%>
    <c:if test="${not empty message}">
        <div class="error">
            <c:out value="${message}"/>
            <c:remove var="message" scope="session"/>
        </div>
    </c:if>

    <table>
        <tr>
            <th>№</th>
            <th>Дата доставки</th>
            <th>Время доставки</th>
            <th>Город</th>
            <th>Адрес</th>
            <th>Телефон</th>
            <th>Имя</th>
            <th>Доп. информация</th>
            <th>Стоимость</th>
            <th>Статус</th>
        </tr>
        <c:forEach var="order" items="${requestScope.ordersList}">
            <tr class="table-row">
                <td><a href="${pageContext.request.contextPath}/admin/order/show/${order.id}">
                    <c:out value="${order.id}"/></a></td>
                <td><fmt:formatDate value="${order.deliveryDate}" pattern="dd MMM"/></td>
                <td><c:out value="${order.deliveryTimeDTO}"/></td>
                <td><c:out value="${order.deliveryAddressDTO.city}"/></td>
                <td><c:out value="${order.deliveryAddressDTO.address}"/></td>
                <td><c:out value="${order.deliveryPhoneDTO.phone}"/></td>
                <td><c:out value="${order.userDTO.name}"/></td>
                <td><c:out value="${order.deliveryInformation}"/></td>
                <td><c:out value="${order.cost}"/></td>
                <td><c:out value="${order.statusDTO}"/></td>
            </tr>
        </c:forEach>
    </table>
    <%--    <form id="delete" action="${pageContext.request.contextPath}/admin/products/delete" method="post">
            <input type="submit" class="register" value="Удалить">
        </form>--%>
</div>

<div class="pagination">
    <c:choose>
        <c:when test="${not empty param.page}">
            <c:set value="${param.page}" var="currentPage"/>
        </c:when>
        <c:otherwise>
            <c:set value="1" var="currentPage"/>
        </c:otherwise>
    </c:choose>
    <a href="?page=${currentPage - 1}">&laquo;</a>
    <c:forEach var="pageNum" begin="1" end="${requestScope.pagesQuantity}">
        <c:choose>
            <c:when test="${currentPage eq pageNum}">
                <a class="active" href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:when>
            <c:otherwise>
                <a href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <a href="?page=${currentPage + 1}">&raquo;</a>
</div>

</body>
</html>