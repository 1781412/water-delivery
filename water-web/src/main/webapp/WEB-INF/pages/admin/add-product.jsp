<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Добавление продукта | Water Delivery</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<div class="news-add-body">
    <div class="header">
        <c:out value="Products"/>
    </div>
    <%--message--%>
    <c:if test="${not empty error}">
        <div class="error">
            <c:out value="${error}"/>
            <c:remove var="error" scope="session"/>
        </div>
    </c:if>
    <div class="form">
        <form:form action="${pageContext.request.contextPath}/admin/product/add" modelAttribute="product"
                   enctype="multipart/form-data" method="post">
            <form:input class="newsheadline" name="product-name" type="text" placeholder="Название продукта"
                   value="${product.name}" path="name"/>
            <form:input class="newsheadline" name="inv-num" type="text" placeholder="Инвентарный номер"
                   value="${product.invNum}" path="invNum"/>
            <form:textarea class="newstext" name="description" placeholder="Описание продукта"
                    value="${product.description}" path="description"/>
            <label for="price">Цена, BYN: </label>
            <form:input id="price" name="price" type="text" placeholder="Цена"
                   value="${product.price}" path="price"/><br>
            <label for="product-image">Фото продукта: </label>
            <form:input path="imageId" cssClass="hidden" value="${product.imageId}" name="imageId"/>
            <form:input cssClass="file" name="product-image" id="product-image" type="file" accept="image/jpeg"
                        path="file"/><br>

            <br><br>
            <form:checkbox cssClass="checkbox" name="available" id="available" path="available"/>
            <label for="available">В продаже: </label>
            <br>
            <c:choose>
                <c:when test="${not empty product.id}">
                    <form:input path="id" cssClass="hidden" value="${product.id}"/>
                    <c:set var="product" value="${product}" scope="session"/>
                    <input type="submit" value="Сохранить"/>
                </c:when>
                <c:otherwise>
                    <input type="submit" value="Добавить продукт"/>
                </c:otherwise>
            </c:choose>

        </form:form>
    </div>
</div>
</body>
</html>
