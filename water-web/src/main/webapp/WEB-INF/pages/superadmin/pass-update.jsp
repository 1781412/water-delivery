<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
    <title>Password update | Water Delivery</title>
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<%--error message--%>
<c:if test="${not empty message}">
    <div class="error">
        <c:out value="${message}"/>
    </div>
</c:if>

<div class="form">
    <form action="${pageContext.request.contextPath}/superadmin/users/pass-update" method="post">
        <input name="id" type="number" value="${id}" class="hidden">
        <input id="password" name="password" type="password" placeholder="Your password.."><br/>

        <input name="pass" type="password" placeholder="Repeat password.."><br/>
        <input type="submit" value="Изменить">
    </form>
</div>

</body>
</html>
