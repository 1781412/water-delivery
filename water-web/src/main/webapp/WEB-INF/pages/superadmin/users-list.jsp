<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<spring:message code="label_users_list" var="labelUsersList"/>
<spring:message code="headline.name" var="headlineName"/>
<spring:message code="headline.role" var="headlineRole"/>
<spring:message code="headline.status" var="headlineStatus"/>
<spring:message code="button.delete" var="buttonDelete"/>

<html>
<head>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
    <title>Пользователи | Water delivery</title>
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>

<h1>${labelUsersList}</h1>

<div class="table">

    <%--message--%>
    <c:if test="${not empty message}">
        <div class="error">
            <c:out value="${message}"/>
            <c:remove var="message" scope="session"/>
        </div>
    </c:if>

<input form="filter" class="hidden" name="page" value="${param.page}">
    <table>
        <tr>
            <th></th>
            <th>E-mail</th>
            <th><c:out value="${headlineName}"/></th>
            <form id="filter" name="filter" action="${pageContext.request.contextPath}/superadmin/users" method="get">
                <th><c:out value="${headlineRole}"/>

                    <select name="role" id="role" class="filter" onchange="document.forms['filter'].submit()">
                        <c:choose>
                            <c:when test="${param.role eq 'all'}">
                                <option selected>all</option>
                            </c:when>
                            <c:otherwise>
                                <option>all</option>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${param.role eq 'ROLE_SUPERADMIN'}">
                                <option value="ROLE_SUPERADMIN" selected>SUPERADMIN</option>
                            </c:when>
                            <c:otherwise>
                                <option value="ROLE_SUPERADMIN">SUPERADMIN</option>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${param.role eq 'ROLE_ADMIN'}">
                                <option value="ROLE_ADMIN" selected>ADMIN</option>
                            </c:when>
                            <c:otherwise>
                                <option value="ROLE_ADMIN">ADMIN</option>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${param.role eq 'ROLE_USER'}">
                                <option value="ROLE_USER" selected>USER</option>
                            </c:when>
                            <c:otherwise>
                                <option value="ROLE_USER">USER</option>
                            </c:otherwise>
                        </c:choose>
                    </select>

                </th>
                <th><c:out value="${headlineStatus}"/>

                    <select name="status" id="status" class="filter" onchange="document.forms['filter'].submit()">
                        <c:choose>
                            <c:when test="${param.status eq 'all'}">
                                <option selected>all</option>
                            </c:when>
                            <c:otherwise>
                                <option>all</option>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${param.status eq 'VALID'}">
                                <option selected>VALID</option>
                            </c:when>
                            <c:otherwise>
                                <option>VALID</option>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${param.status eq 'INVALID'}">
                                <option selected>INVALID</option>
                            </c:when>
                            <c:otherwise>
                                <option>INVALID</option>
                            </c:otherwise>
                        </c:choose>
                    </select>

                </th>
            </form>
        </tr>
        <c:forEach var="user" items="${requestScope.users}">
            <tr class="table-row">
                <td><input form="delete" class="checkbox" type="checkbox" name="id" value="${user.id}"></td>
                <td><a href="${pageContext.request.contextPath}/superadmin/users/show?id=${user.id}">
                    <c:out value="${user.login}"/></a></td>
                <td><c:out value="${user.name} ${user.lastName}"/></td>
                <td><c:out value="${user.role}"/></td>
                <td><c:out value="${user.userStatus}"/></td>
            </tr>
        </c:forEach>
    </table>
    <form id="delete" action="${pageContext.request.contextPath}/superadmin/users/delete" method="post">
        <input type="submit" class="register" value="${buttonDelete}">
    </form>
</div>

<div class="pagination">
    <c:choose>
        <c:when test="${not empty param.page}">
            <c:set value="${param.page}" var="currentPage"/>
        </c:when>
        <c:otherwise>
            <c:set value="1" var="currentPage"/>
        </c:otherwise>
    </c:choose>
    <a href="?page=${currentPage - 1}">&laquo;</a>
    <c:forEach var="pageNum" begin="1" end="${requestScope.pagesQuantity}">
        <c:choose>
            <c:when test="${currentPage eq pageNum}">
                <a class="active" href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:when>
            <c:otherwise>
                <a href="?page=${pageNum}"><c:out value="${pageNum}"/></a>
            </c:otherwise>
        </c:choose>
    </c:forEach>
    <a href="?page=${currentPage + 1}">&raquo;</a>
</div>

</body>
</html>