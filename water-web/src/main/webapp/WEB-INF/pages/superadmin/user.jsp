<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>User | Water Delivery</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<h1><c:out value="${userDTO.name} ${userDTO.middleName} ${userDTO.lastName}"/></h1>
<input form="roleChange" name="id" type="number" value="${userDTO.id}" class="hidden">
<input form="statusChange" name="id" type="number" value="${userDTO.id}" class="hidden">
<input form="delete" name="id" type="number" value="${userDTO.id}" class="hidden">

<%--message--%>
<c:if test="${not empty message}">
    <div class="error">
        <c:out value="${message}"/>
        <c:remove var="message" scope="session"/>
    </div>
</c:if>


<div class="table">
    <table>
        <tr>
            <td>id:</td>
            <td><c:out value="${userDTO.id}"/></td>
        </tr>
        <tr>
            <td>Login:</td>
            <td><c:out value="${userDTO.login}"/></td>
        </tr>
        <tr>
            <td>Password:</td>
            <td><a href="${pageContext.request.contextPath}/superadmin/users/pass-update?id=${userDTO.id}">
                изменить</a></td>
        </tr>
        <tr>
            <td>Role:</td>
            <td class="user-property">
                <form id="roleChange" name="roleChange"
                      action="${pageContext.request.contextPath}/superadmin/users/role-change"
                      method="post"><c:out value="${userDTO.role}"/>
                    <select name="role" class="status-change" onchange="document.forms['roleChange'].submit()">
                        <option>изменить</option>
                        <option value="ROLE_SUPERADMIN">SUPERADMIN</option>
                        <option value="ROLE_ADMIN">ADMIN</option>
                        <option value="ROLE_USER">USER</option>
                    </select>
                </form>
            </td>
        </tr>
        <tr>
            <td>Status:</td>
            <td class="user-property">
                <form id="statusChange" name="statusChange"
                      action="${pageContext.request.contextPath}/superadmin/users/status-change"
                method="post">
                    <c:out value="${userDTO.userStatus}"/>
                    <select name="status" class="status-change" onchange="document.forms['statusChange'].submit()">
                        <option>изменить</option>
                        <option>VALID</option>
                        <option>INVALID</option>
                    </select>
                </form>
            </td>
        </tr>
        <tr>
            <td>City:</td>
            <td><c:out value="${userDTO.city}"/></td>
        </tr>
        <tr>
            <td>Address:</td>
            <td>
                <c:forEach var="address" items="${userDTO.addresses}">
                    <c:out value="г. ${address.city}, ${address.address}"/><br>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <td>Phone:</td>
            <td>
                <c:forEach var="phone" items="${userDTO.phones}">
                    <c:out value="${phone.phone}"/><br>
                </c:forEach>
            </td>
        </tr>
        <tr>
            <td>Additional information:</td>
            <td><c:out value="${userDTO.additionalInfo}"/></td>
        </tr>
    </table>
    <form id="delete" action="${pageContext.request.contextPath}/superadmin/users/delete" method="post">
        <input type="submit" class="register" value="Удалить">
    </form>
</div>
</body>
</html>