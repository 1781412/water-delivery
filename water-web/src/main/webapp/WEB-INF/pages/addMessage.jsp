<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Contact us | Water Delivery</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">
</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
</div>
<div class="news-add-body">
    <div class="header">
        <c:out value="Contact us"/>
    </div>
    <%--message--%>
    <c:if test="${not empty error}">
        <div class="error">
            <c:out value="${error}"/>
        </div>
    </c:if>
    <div class="form">
        <form:form action="${pageContext.request.contextPath}/message"
                   method="post" modelAttribute="messageDTO">
            <form:input cssClass="newsheadline" name="email" type="text" placeholder="email"
                        value="${post.headline}" path="email"/>
            <form:input cssClass="newsheadline" name="subject" type="text" placeholder="subject"
                        value="${post.headline}" path="subject"/>
            <form:textarea class="newstext" name="message" placeholder="Текст сообщения" path="message" />

            <%--<label for="date">Дата и время публикации: </label>
            <c:choose>
                <c:when test="${not empty requestScope.date}">
                    <c:set value="${requestScope.date}" var="pubDate"/>
                </c:when>
                <c:otherwise>
                    <fmt:formatDate value="${post.publicationDate}" pattern="dd-MM-yyyy HH:mm" var="pubDate"/>
                </c:otherwise>
            </c:choose>
            <form:input name="date" id="date" type="datetime" path="publicationDate" value="${pubDate}"/>--%>
            <br><br>
            <input type="submit" value="Отправить"/>
        </form:form>
    </div>
</div>
</body>
</html>
