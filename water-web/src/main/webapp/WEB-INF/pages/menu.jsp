<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<body>
<a href="${pageContext.request.contextPath}/message"><spring:message code="menu.message"/></a> |
<a href="${pageContext.request.contextPath}/news"><spring:message code="menu.news"/></a> |
<sec:authorize access="hasRole('ROLE_USER') and isAuthenticated()">
    <a href="${pageContext.request.contextPath}/user/shop"><spring:message code="menu.shop"/></a> |
    <a href="${pageContext.request.contextPath}/user/basket"><spring:message code="menu.basket"/></a> |
    <a href="${pageContext.request.contextPath}/user/orders"><spring:message code="menu.my.orders"/></a> |
</sec:authorize>
<sec:authorize access="hasAnyRole('ROLE_ADMIN', 'ROLE_SUPERADMIN') and isAuthenticated()" >
    <a href="${pageContext.request.contextPath}/admin/messages"><spring:message code="menu.messages"/></a> |
    <a href="${pageContext.request.contextPath}/admin/orders"><spring:message code="menu.orders"/></a> |
    <a href="${pageContext.request.contextPath}/admin/products?page=1"><spring:message code="menu.products"/></a> |
    <a href="${pageContext.request.contextPath}/admin/news?page=1"><spring:message code="menu.news.edit"/></a> |
</sec:authorize>
<sec:authorize access="hasRole('ROLE_SUPERADMIN') and isAuthenticated()">
    <a href="${pageContext.request.contextPath}/superadmin/users?page=1&role=all&status=all">
        <spring:message code="menu.users"/></a> |
</sec:authorize>
<sec:authorize access="isAuthenticated()">
    <a href="${pageContext.request.contextPath}/logout"><spring:message code="menu.logout"/></a>
</sec:authorize>
<sec:authorize access="isAnonymous()">
    <a href="${pageContext.request.contextPath}/login"><spring:message code="menu.login"/></a>
</sec:authorize>
</body>
</html>
