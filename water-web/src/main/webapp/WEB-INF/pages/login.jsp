<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<html>
<head>
    <title>Login | Water Delivery</title>

    <link href="${pageContext.request.contextPath}/resources/css/style.css" rel="stylesheet">

</head>
<body>
<div class="menu">
    <jsp:include page="/WEB-INF/pages/menu.jsp"/>
    <button form="lang" class="lang" type="submit" name="lang" value="ru_RU">RU</button>
    <button form="lang" class="lang" type="submit" name="lang" value="en_US">EN</button>
    <form hidden id="lang"></form>
</div>

<div class="header">
    <spring:message code="headline.login"/>
</div>
<%--message--%>
<c:if test="${not empty message}">
    <div class="error">
        <c:out value="${message}"/>
        <%--<c:remove var="message" scope="session"/>--%>
    </div>
</c:if>
<div class="form">
    <form action="${pageContext.request.contextPath}/login" method="post">
        <c:if test="${param['error']}">
            <div class="error">
                <spring:message code="warning.credentials.invalid"/>
            </div>
        </c:if>
        <spring:message code="label.email" var="labelEmail"/>
        <input id="login" name="username" type="email" placeholder="${labelEmail}" ><br/>
        <spring:message code="label.password" var="labelPassword"/>
        <input name="password" type="password" placeholder="${labelPassword}"><br/>
        <spring:message code="button.login" var="buttonLogin"/>
        <input type="submit" value="${buttonLogin}">
    </form>
</div>
<div class="form">
    <form action="${pageContext.request.contextPath}/new-user">
        <spring:message code="label.or"/><br>
        <spring:message code="label.register" var="labelRegister"/>
        <input class="register" type="submit" value="${labelRegister}">
    </form>
</div>

</body>
</html>
