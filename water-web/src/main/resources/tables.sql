drop table if exists T_NEWS;
drop table if exists T_NEWS_FILE;
drop table if exists T_ORDER;
drop table if exists T_PRODUCT;
drop table if exists T_PRODUCT_FILE;
drop table if exists T_USER;
drop table if exists T_USER_ADDRESS;
drop table if exists T_USER_INFORMATION;
drop table if exists T_USER_PHONE;
create table T_BASKET (F_ID integer not null auto_increment, F_ORDER_ID integer, F_QUANTITY integer, F_USER_ID integer, F_PRODUCT_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_NEWS (F_ID integer not null auto_increment, F_HEADLINE varchar(255), F_NEWS_TEXT varchar(2000), F_PUBLICATION_DATE datetime, F_VISIBLE bit, F_USER_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_NEWS_FILE (F_POST_ID integer not null, F_FILE_NAME varchar(255), F_LOCATION varchar(255), primary key (F_POST_ID)) engine=MyISAM;
create table T_ORDER (F_ID integer not null auto_increment, F_COST decimal(19,2), F_CREATION_DATE TIMESTAMP, F_DELIVERY_DATE datetime, F_DELIVERY_INFORMATION varchar(255), F_DELIVERY_TIME varchar(255), F_STATUS varchar(255), F_DELIVERY_ADDRESS integer, F_DELIVERY_PHONE integer, F_USER_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_PRODUCT (F_ID integer not null auto_increment, F_AVAILABLE bit, F_CREATION_DATE TIMESTAMP, F_DESCRIPTION varchar(255), F_INV_NUM varchar(255), F_NAME varchar(255), F_PRICE decimal(19,2), F_USER_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_PRODUCT_FILE (F_PRODUCT_ID integer not null, F_FILE_NAME varchar(255), F_LOCATION varchar(255), primary key (F_PRODUCT_ID)) engine=MyISAM;
create table T_USER (F_ID integer not null auto_increment, F_EMAIL varchar(255), F_NAME varchar(255), F_PASSWORD varchar(255), F_ROLE varchar(255), F_USER_STATUS varchar(255), primary key (F_ID)) engine=MyISAM;
create table T_USER_ADDRESS (F_ID integer not null auto_increment, F_ADDRESS varchar(255), F_CITY varchar(255), F_USER_ID integer, primary key (F_ID)) engine=MyISAM;
create table T_USER_INFORMATION (F_USER_ID integer not null, F_ADDITIONAL_INFORMATION varchar(2000), F_LAST_NAME varchar(255), F_MIDDLE_NAME varchar(255), F_REGISTRATION_DATE TIMESTAMP, primary key (F_USER_ID)) engine=MyISAM;
create table T_USER_PHONE (F_ID integer not null auto_increment, F_PHONE varchar(255), F_USER_ID integer, primary key (F_ID)) engine=MyISAM;
alter table T_BASKET add index FK2BAD6411A3B78251 (F_PRODUCT_ID), add constraint FK2BAD6411A3B78251 foreign key (F_PRODUCT_ID) references T_PRODUCT (F_ID);
alter table T_BASKET add index FK2BAD64113AECC3B1 (F_ORDER_ID), add constraint FK2BAD64113AECC3B1 foreign key (F_ORDER_ID) references T_ORDER (F_ID);
alter table T_NEWS add index FK94B64FDEEE82AA43 (F_USER_ID), add constraint FK94B64FDEEE82AA43 foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_ORDER add index FK22763835623E589 (F_DELIVERY_ADDRESS), add constraint FK22763835623E589 foreign key (F_DELIVERY_ADDRESS) references T_USER_ADDRESS (F_ID);
alter table T_ORDER add index FK2276383EE82AA43 (F_USER_ID), add constraint FK2276383EE82AA43 foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_ORDER add index FK2276383A8C1CBD (F_DELIVERY_PHONE), add constraint FK2276383A8C1CBD foreign key (F_DELIVERY_PHONE) references T_USER_PHONE (F_ID);
alter table T_PRODUCT add index FK4B5D6BE4EE82AA43 (F_USER_ID), add constraint FK4B5D6BE4EE82AA43 foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_USER_ADDRESS add index FK3C31496BEE82AA43 (F_USER_ID), add constraint FK3C31496BEE82AA43 foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_USER_PHONE add index FK8DD0E0A5EE82AA43 (F_USER_ID), add constraint FK8DD0E0A5EE82AA43 foreign key (F_USER_ID) references T_USER (F_ID);

/*hibernate5*/

drop table if exists T_BASKET;
drop table if exists T_FILE;
drop table if exists T_MESSAGE;
drop table if exists T_NEWS;
drop table if exists T_ORDER;
drop table if exists T_PRODUCT;
drop table if exists T_USER;
drop table if exists T_USER_ADDRESS;
drop table if exists T_USER_INFORMATION;
drop table if exists T_USER_PHONE;
create table T_BASKET (F_ID integer not null auto_increment, F_ORDER_ID integer, F_QUANTITY integer, F_USER_ID integer, F_PRODUCT_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_FILE (F_ID integer not null auto_increment, F_FILE_NAME varchar(255), F_LOCATION varchar(255), primary key (F_ID)) engine=MyISAM;
create table T_MESSAGE (F_ID integer not null auto_increment, F_DATE datetime, F_EMAIL varchar(255), F_MESSAGE varchar(2000), F_SUBJECT varchar(255), F_USER_ID integer, primary key (F_ID)) engine=MyISAM;
create table T_NEWS (F_ID integer not null auto_increment, F_HEADLINE varchar(255), F_NEWS_TEXT varchar(2000), F_PUBLICATION_DATE datetime, F_VISIBLE bit, F_FILE_ID integer, F_USER_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_ORDER (F_ID integer not null auto_increment, F_COST decimal(19,2), F_CREATION_DATE TIMESTAMP, F_DELIVERY_DATE datetime, F_DELIVERY_INFORMATION varchar(255), F_DELIVERY_TIME varchar(255), F_STATUS varchar(255), F_DELIVERY_ADDRESS integer, F_DELIVERY_PHONE integer, F_USER_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_PRODUCT (F_ID integer not null auto_increment, F_AVAILABLE bit, F_CREATION_DATE TIMESTAMP, F_DESCRIPTION varchar(255), F_INV_NUM varchar(255), F_NAME varchar(255), F_PRICE decimal(19,2), F_FILE_ID integer, F_USER_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_USER (F_ID integer not null auto_increment, F_EMAIL varchar(255), F_NAME varchar(255), F_PASSWORD varchar(255), F_ROLE varchar(255), F_USER_STATUS varchar(255), primary key (F_ID)) engine=MyISAM;
create table T_USER_ADDRESS (F_ID integer not null auto_increment, F_ADDRESS varchar(255), F_CITY varchar(255), F_USER_ID integer, primary key (F_ID)) engine=MyISAM;
create table T_USER_INFORMATION (F_USER_ID integer not null auto_increment, F_ADDITIONAL_INFORMATION varchar(2000), F_LAST_NAME varchar(255), F_MIDDLE_NAME varchar(255), F_REGISTRATION_DATE datetime, primary key (F_USER_ID)) engine=MyISAM;
create table T_USER_PHONE (F_ID integer not null auto_increment, F_PHONE varchar(255), F_USER_ID integer, primary key (F_ID)) engine=MyISAM;
alter table T_BASKET add constraint FK6tmwxix65d7fboaa2v2dnjgbg foreign key (F_PRODUCT_ID) references T_PRODUCT (F_ID);
alter table T_BASKET add constraint FKbd6evawvockylpa90ofxjdw86 foreign key (F_ORDER_ID) references T_ORDER (F_ID);
alter table T_MESSAGE add constraint FK47wdmkd3ru7s99qvuj2rbxe64 foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_NEWS add constraint FKcl6drbcsl84kj2mcpyy7axabv foreign key (F_FILE_ID) references T_FILE (F_ID);
alter table T_NEWS add constraint FKd5y4mg1tcpsyi2dcla37gka5m foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_ORDER add constraint FKm153judx8bmuwk7thjlx8vsoj foreign key (F_DELIVERY_ADDRESS) references T_USER_ADDRESS (F_ID);
alter table T_ORDER add constraint FK208f31kfq42jytwsip1ph0q7i foreign key (F_DELIVERY_PHONE) references T_USER_PHONE (F_ID);
alter table T_ORDER add constraint FK1dpyp0hr6159qy32fho9na0ya foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_PRODUCT add constraint FKrgsihxbru150pj7spo55ykqxj foreign key (F_FILE_ID) references T_FILE (F_ID);
alter table T_PRODUCT add constraint FK5k0o2yka1kfhil27wdbvpvii2 foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_USER_ADDRESS add constraint FKs3kpkdij63avpod0q3eu0ovkq foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_USER_PHONE add constraint FK78batikyrhn26c6gt2c3hl314 foreign key (F_USER_ID) references T_USER (F_ID);

/*hibernate5*/

drop table if exists hibernate_sequence;
drop table if exists T_BASKET;
drop table if exists T_NEWS;
drop table if exists T_NEWS_FILE;
drop table if exists T_ORDER;
drop table if exists T_PRODUCT;
drop table if exists T_PRODUCT_FILE;
drop table if exists T_USER;
drop table if exists T_USER_ADDRESS;
drop table if exists T_USER_INFORMATION;
drop table if exists T_USER_PHONE;
create table hibernate_sequence (next_val bigint) engine=MyISAM;
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
insert into hibernate_sequence values ( 1 );
create table T_BASKET (F_ID integer not null auto_increment, F_ORDER_ID integer, F_QUANTITY integer, F_USER_ID integer, F_PRODUCT_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_NEWS (F_ID integer not null auto_increment, F_HEADLINE varchar(255), F_NEWS_TEXT varchar(2000), F_PUBLICATION_DATE datetime, F_VISIBLE bit, F_FILE_ID integer, F_USER_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_NEWS_FILE (F_POST_ID integer not null, F_FILE_NAME varchar(255), F_LOCATION varchar(255), primary key (F_POST_ID)) engine=MyISAM;
create table T_ORDER (F_ID integer not null auto_increment, F_COST decimal(19,2), F_CREATION_DATE datetime, F_DELIVERY_DATE datetime, F_DELIVERY_INFORMATION varchar(255), F_DELIVERY_TIME varchar(255), F_STATUS varchar(255), F_DELIVERY_ADDRESS integer, F_DELIVERY_PHONE integer, F_USER_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_PRODUCT (F_ID integer not null auto_increment, F_AVAILABLE bit, F_CREATION_DATE datetime, F_DESCRIPTION varchar(255), F_INV_NUM varchar(255), F_NAME varchar(255), F_PRICE decimal(19,2), F_USER_ID integer not null, primary key (F_ID)) engine=MyISAM;
create table T_PRODUCT_FILE (F_PRODUCT_ID integer not null, F_FILE_NAME varchar(255), F_LOCATION varchar(255), primary key (F_PRODUCT_ID)) engine=MyISAM;
create table T_USER (F_ID integer not null auto_increment, F_EMAIL varchar(255), F_NAME varchar(255), F_PASSWORD varchar(255), F_ROLE varchar(255), F_USER_STATUS varchar(255), primary key (F_ID)) engine=MyISAM;
create table T_USER_ADDRESS (F_ID integer not null auto_increment, F_ADDRESS varchar(255), F_CITY varchar(255), F_USER_ID integer, primary key (F_ID)) engine=MyISAM;
create table T_USER_INFORMATION (F_USER_ID integer not null, F_ADDITIONAL_INFORMATION varchar(2000), F_LAST_NAME varchar(255), F_MIDDLE_NAME varchar(255), F_REGISTRATION_DATE datetime, primary key (F_USER_ID)) engine=MyISAM;
create table T_USER_PHONE (F_ID integer not null auto_increment, F_PHONE varchar(255), F_USER_ID integer, primary key (F_ID)) engine=MyISAM;
alter table T_BASKET add constraint FK6tmwxix65d7fboaa2v2dnjgbg foreign key (F_PRODUCT_ID) references T_PRODUCT (F_ID);
alter table T_BASKET add constraint FKbd6evawvockylpa90ofxjdw86 foreign key (F_ORDER_ID) references T_ORDER (F_ID);
alter table T_NEWS add constraint FKjbb7mxyqvuaiaasdrp8n4ls2p foreign key (F_FILE_ID) references T_NEWS_FILE (F_POST_ID);
alter table T_NEWS add constraint FKd5y4mg1tcpsyi2dcla37gka5m foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_ORDER add constraint FKm153judx8bmuwk7thjlx8vsoj foreign key (F_DELIVERY_ADDRESS) references T_USER_ADDRESS (F_ID);
alter table T_ORDER add constraint FK208f31kfq42jytwsip1ph0q7i foreign key (F_DELIVERY_PHONE) references T_USER_PHONE (F_ID);
alter table T_ORDER add constraint FK1dpyp0hr6159qy32fho9na0ya foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_PRODUCT add constraint FK5k0o2yka1kfhil27wdbvpvii2 foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_USER_ADDRESS add constraint FKs3kpkdij63avpod0q3eu0ovkq foreign key (F_USER_ID) references T_USER (F_ID);
alter table T_USER_PHONE add constraint FK78batikyrhn26c6gt2c3hl314 foreign key (F_USER_ID) references T_USER (F_ID);

INSERT INTO t_user VALUES (1,'superadmin@admin.com', 'Коля','$2a$06$v2xyvgqEF5eHnJljFU07zO7t2rnht.il3BYyrAeLv7Xsbk17mDe6O', 'SUPERADMIN', 'VALID');
INSERT INTO T_USER_INFORMATION VALUES (1,'add inf', 'Pupkin', 'вапы',null);

UPDATE T_USER SET F_EMAIL='superadmin@admin.com',
  F_PASSWORD='$2a$06$v2xyvgqEF5eHnJljFU07zO7t2rnht.il3BYyrAeLv7Xsbk17mDe6O'
WHERE F_ID = 1;

