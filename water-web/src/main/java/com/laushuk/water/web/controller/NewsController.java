package com.laushuk.water.web.controller;

import com.laushuk.water.service.NewsService;
import com.laushuk.water.service.model.PostDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Controller
public class NewsController {

    private static final Logger log = Logger.getLogger(NewsController.class);

    private final NewsService newsService;

    @Autowired
    public NewsController(NewsService newsService) {
        this.newsService = newsService;
    }

    @RequestMapping(value = "/admin/news/add", method = RequestMethod.GET)
    public String showPostAddForm(@ModelAttribute("post") PostDTO post) {
        return "admin/add-news";
    }

    @RequestMapping(value = "/admin/news/add", method = RequestMethod.POST)
    public String savePost(
            @ModelAttribute("post") PostDTO post
    ) throws IOException {
        newsService.save(post);
        return "redirect:/admin/news?page=1";
    }

    @RequestMapping(value = "/admin/news", method = RequestMethod.GET)
    public String showNewsList(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                               Model model) {
        int postPerPage = 10;
        List<PostDTO> news = newsService.getPaginatedPosts(page,0,postPerPage);
        Long pagesQuantity = newsService.getPagesQuantity(0, postPerPage);
        model.addAttribute("newsList", news);
        model.addAttribute("pagesQuantity", pagesQuantity);
        return "admin/news-list";
    }

    @RequestMapping(value = "/news", method = RequestMethod.GET)
    public String showNews(@RequestParam(value = "page", required = false, defaultValue = "1") int page,
                           Model model) {
        int postPerPage = 3;
        List<PostDTO> news = newsService.getPaginatedPosts(page,1,postPerPage);
        Long pagesQuantity = newsService.getPagesQuantity(1, postPerPage);
        model.addAttribute("newsList", news);
        model.addAttribute("pagesQuantity", pagesQuantity);
        return "user/news";
    }

    @RequestMapping(value = "/admin/news/show/{id}", method = RequestMethod.GET)
    public String showPost(@PathVariable("id") int id, Model model) {
        PostDTO postDTO = newsService.getPostById(id);
        model.addAttribute("post", postDTO);
        return "admin/post";
    }

    @RequestMapping(value = "/admin/news/edit/{id}", method = RequestMethod.GET)
    public String editPost(@PathVariable("id") int id, Model model) {
        PostDTO postDTO = newsService.getPostById(id);
        model.addAttribute("post", postDTO);
        return "admin/add-news";
    }

    @RequestMapping(value = "/admin/news/delete")
    public String deletePost(
            @RequestParam("id") int [] ids) {
        newsService.deleteByIds(ids);
        return "redirect:/admin/news?page=1";
    }

    @RequestMapping(value = "/admin/news/publish/{id}", method = RequestMethod.GET)
    public String publishPost(@PathVariable("id") int id) {
        newsService.publish(id);
        return "redirect:/admin/news?page=1";
    }
}
