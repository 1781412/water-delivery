package com.laushuk.water.web.controller;

import com.laushuk.water.service.FileService;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;
import java.nio.charset.Charset;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Controller
public class DownloadController {
    private static final Logger log = Logger.getLogger(DownloadController.class);

    private FileService fileService;

    @Autowired
    public DownloadController(FileService fileService) {
        this.fileService = fileService;
    }

    @RequestMapping(value = "/download/{id}")
    public void download(HttpServletResponse resp, @PathVariable long id) throws IOException {

        File file = fileService.getFileById(id);

        if (!file.exists()) {
            String errorMessage = "Sorry. The file you are looking for does not exist";
            log.info(errorMessage);
            OutputStream outputStream = resp.getOutputStream();
            outputStream.write(errorMessage.getBytes(Charset.forName("UTF-8")));
            outputStream.close();
            return;
        }

        String mimeType = URLConnection.guessContentTypeFromName(file.getName());
        if (mimeType == null) {
            log.info("Mime type is not detectable, will take default");
            mimeType = "application/octet-stream";
        }

        log.info("Mime type : " + mimeType);
        resp.setContentType(mimeType);

        resp.setHeader("Content-Disposition", String.format("inline; filename=\"%s\"", file.getName()));
        resp.setContentLength((int) file.length());

        try (
                FileInputStream fileInputStream = new FileInputStream(file);
                InputStream inputStream = new BufferedInputStream(fileInputStream)
        ) {
            FileCopyUtils.copy(inputStream, resp.getOutputStream());
        }
    }
}
