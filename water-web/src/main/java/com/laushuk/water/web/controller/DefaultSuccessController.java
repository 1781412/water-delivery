package com.laushuk.water.web.controller;

import com.laushuk.water.repository.model.UserRole;
import com.laushuk.water.service.model.AppUserPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Controller
public class DefaultSuccessController {

    @RequestMapping(value = "/filter", method = RequestMethod.GET)
    public String filterDirectedUrl() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        System.out.println(principal);
        if (principal.getRole() == UserRole.ROLE_USER) {
            return "redirect:/user/shop";
        } else if (principal.getRole() == UserRole.ROLE_ADMIN) {
            return "redirect:/admin/orders";
        } else if (principal.getRole() == UserRole.ROLE_SUPERADMIN) {
            return "redirect:/admin/orders";
        } else return "redirect:/login";
    }
}
