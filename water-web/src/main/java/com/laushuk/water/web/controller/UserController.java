package com.laushuk.water.web.controller;

import com.laushuk.water.service.UserService;
import com.laushuk.water.service.model.UserDTO;
import com.laushuk.water.service.model.UserDTOFull;
import com.laushuk.water.service.model.UserRoleDTO;
import com.laushuk.water.service.model.UserStatusDTO;
import com.laushuk.water.web.controller.validator.UserValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
public class UserController {

    private final UserService userService;
    private final UserValidator userValidator;

    @Autowired
    public UserController(UserService userService, UserValidator userValidator) {
        this.userService = userService;
        this.userValidator = userValidator;
    }


    @RequestMapping(value = "/superadmin/users", method = RequestMethod.GET)
    public String showUsersListPage(
            @RequestParam("page") int page,
            @RequestParam("status") String statusStr,
            @RequestParam("role") String roleStr,
            Model model
    ) {
        UserStatusDTO status = null;
        if (!statusStr.equalsIgnoreCase("all")) status = UserStatusDTO.valueOf(statusStr);
        UserRoleDTO role = null;
        if (!roleStr.equalsIgnoreCase("all")) role = UserRoleDTO.valueOf(roleStr);
        int itemsPerPage = 10;

        List<UserDTO> users = userService.getPaginatedUsers(page, status, role, itemsPerPage);
        Long pagesQuantity = userService.getPagesQuantity(status, role, itemsPerPage);
        model.addAttribute("users", users);
        model.addAttribute("pagesQuantity", pagesQuantity);
        return "superadmin/users-list";
    }


    @RequestMapping(value = "/superadmin/users/delete", method = RequestMethod.POST)
    public String deleteUser(@RequestParam(value = "id", required = false, defaultValue = "none") String[] ids,
                             final RedirectAttributes redirectAttributes) {
        int result;
        if (!ids[0].equals("null")) {
            result = userService.deleteByIds(ids);
            redirectAttributes.addFlashAttribute("message", "Удалено " + result + " пользователей");
        }
        return "redirect:/superadmin/users?page=1&role=all&status=all";
    }


    @RequestMapping(value = "/superadmin/users/show", method = RequestMethod.GET)
    public String showUser(@RequestParam("id") int id, Model model) {
        UserDTOFull userDTOFull = userService.get(id);
        model.addAttribute("userDTO", userDTOFull);
        return "superadmin/user";
    }


    @RequestMapping(value = "/superadmin/users/role-change", method = RequestMethod.POST)
    public String changeUserRole(
            @RequestParam("id") int id,
            @RequestParam("role") String role
    ) {
        int result = userService.updateRole(id, role);
        //добваить сообщение в зависимости от результата
        return "redirect:/superadmin/users/show?id=" + id;
    }


    @RequestMapping(value = "/superadmin/users/status-change", method = RequestMethod.POST)
    public String changeUserStatus(
            @RequestParam("id") int id,
            @RequestParam("status") String status
    ) {
        int result = userService.updateStatus(id, status);
        //добваить сообщение в зависимости от результата
        return "redirect:/superadmin/users/show?id=" + id;
    }


    @RequestMapping(value = "/superadmin/users/pass-update", method = RequestMethod.GET)
    public String showPassUpdateForm(@RequestParam("id") int id, Model model) {
        model.addAttribute("id", id);
        return "superadmin/pass-update";
    }

    @RequestMapping(value = "/superadmin/users/pass-update", method = RequestMethod.POST)
    public String passUpdate(
            @RequestParam("id") int id,
            @RequestParam("password") String password,
            @RequestParam("pass") String pass,
            Model model
    ) {
        if (pass.equals(password)) {
            int result = userService.updatePassword(id, pass);
            //добавить сообщение в зависимости от результата
            return "redirect:/superadmin/users/show?id=" + id;
        } else {
            model.addAttribute("id", id);
            return "superadmin/pass-update";
        }
    }

    @RequestMapping(value = "/new-user", method = RequestMethod.GET)
    public String addUserForm(Model model) {
        UserDTO userDTO = new UserDTO();
        model.addAttribute("userDTO", userDTO);
        return "user/register";
    }

    @RequestMapping(value = "/new-user", method = RequestMethod.POST)
    public String addUser(
            @ModelAttribute("userDTO") UserDTO userDTO,
            final RedirectAttributes redirectAttributes,
            BindingResult result
    ) {
        userValidator.validate(userDTO, result);
        if (!result.hasErrors()) {
            userDTO.setRole(UserRoleDTO.ROLE_USER);
            userDTO.setUserStatus(UserStatusDTO.VALID);
            int save = userService.save(userDTO);
            if (save == -1){
                result.addError(new ObjectError("login","already exists"));
            } else if (save == -2){
                result.addError(new ObjectError("error","error"));
            }
        }
        if(!result.hasErrors()){
            redirectAttributes.addFlashAttribute("message", "Success. Account created");
            return "redirect:/";
        } else return "user/register";
    }

}
