package com.laushuk.water.web.controller.validator;

import com.laushuk.water.service.model.OrderSubmitDTO;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by Piotr Laushuk on 01.09.2017. E-mail: 1781412@gmail.com
 */
@Component
public class OrderValidator implements Validator {

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(OrderSubmitDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        OrderSubmitDTO order = (OrderSubmitDTO) o;

        if (order.getAddressInput().equals("new") && order.getCity().equals("")) {
            errors.rejectValue("city", "error.city.not.selected", "please, select the city");
        }
        if (order.getAddressInput().equals("new") && order.getAddress().equals("")) {
            errors.rejectValue("address", "error.address.not.selected", "please, input address");
        }
        if (order.getPhoneInput().equals("new") && order.getPhone().equals("")) {
            errors.rejectValue("phone", "error.phone.not.selected", "please, input phone");
        }
    }
}
