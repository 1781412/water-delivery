package com.laushuk.water.web.controller;

import com.laushuk.water.service.MessageService;
import com.laushuk.water.service.NewsService;
import com.laushuk.water.service.model.MessageDTO;
import com.laushuk.water.service.model.PostDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Controller
public class MessageController {

    private final MessageService messageService;

    @Autowired
    public MessageController(MessageService messageService) {
        this.messageService = messageService;
    }

    @RequestMapping(value = "/message", method = RequestMethod.GET)
    public String showPostAddForm(@ModelAttribute("messageDTO") MessageDTO message) {
        return "addMessage";
    }

    @RequestMapping(value = "/message", method = RequestMethod.POST)
    public String savePost(@ModelAttribute("messageDTO") MessageDTO message, final RedirectAttributes redirectAttributes) {
        messageService.save(message);
        redirectAttributes.addFlashAttribute("message", "Сообщение отправлено");
        return "redirect:/";
    }

    @RequestMapping(value = "/admin/messages", method = RequestMethod.GET)
    public String showNewsList(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                               Model model) {
        int perPage = 10;
        List<MessageDTO> messages = messageService.getPaginated(page,perPage);
        Long pagesQuantity = messageService.getPagesQuantity(perPage);
        model.addAttribute("messages", messages);
        model.addAttribute("pagesQuantity", pagesQuantity);
        return "admin/message-list";
    }

    @RequestMapping(value = "/admin/messages/show/{id}", method = RequestMethod.GET)
    public String showMessage(@PathVariable("id") int id, Model model) {
        MessageDTO messageDTO = messageService.getById(id);
        model.addAttribute("messageDTO", messageDTO);
        return "admin/message";
    }

    @RequestMapping(value = "/admin/messages/delete")
    public String deleteMessage(
            @RequestParam("id") int [] ids) {
        messageService.deleteByIds(ids);
        return "redirect:/admin/messages?page=1";
    }
}
