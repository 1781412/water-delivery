package com.laushuk.water.web.controller;

import com.laushuk.water.service.BasketService;
import com.laushuk.water.service.ProductService;
import com.laushuk.water.service.model.BasketDTO;
import com.laushuk.water.service.model.BigBasketDTO;
import com.laushuk.water.service.model.ProductDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.Set;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Controller
@RequestMapping(value = "/user")
public class BasketController {
    private static final Logger logger = Logger.getLogger(BasketController.class);

    private ProductService productService;
    private BasketService basketService;

    @Autowired
    public BasketController(ProductService productService, BasketService basketService) {
        this.productService = productService;
        this.basketService = basketService;
    }

    @RequestMapping(value = "/shop", method = RequestMethod.GET)
    public String showShop(@RequestParam(value = "page", required = false, defaultValue = "1") int page, Model model) {
        int itemsPerPage = 6;
        List<ProductDTO> productsList = productService.getPaginatedItems(page, 1, itemsPerPage);
        Long pagesQuantity = productService.getPagesQuantity(1, itemsPerPage);
        model.addAttribute("productsList", productsList);
        model.addAttribute("pagesQuantity", pagesQuantity);
        return "user/shop";
    }

    @RequestMapping(value = "/shop", method = RequestMethod.POST)
    public String addToBasket(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            @RequestParam(value = "productId") int productId,
            @RequestParam(value = "itemsQuantity") int itemsQuantity,
            Model model
    ) {
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.setProductId(productId);
        basketDTO.setQuantity(itemsQuantity);
        int result = basketService.add(basketDTO);
        if (result > 0) {
            model.addAttribute("message",
                    "Добавлено в <a href=\"/user/basket\">корзину</a>");
        } else {
            model.addAttribute("message", "Ошибка при добавлении в корзину. Обратитесь к администратору.");
        }
        return showShop(page, model);
    }

    @RequestMapping(value = "/basket")
    public String showBasket(Model model) {
        BigBasketDTO bigBasketDTO = basketService.getBigBasket();
        Set<BasketDTO> items = bigBasketDTO.getBasketDTOS();
        model.addAttribute("basket", items);
        model.addAttribute("cost", bigBasketDTO.getTotalCost());
        return "user/basket";
    }

    @RequestMapping(value = "/basket/delete/{id}", method = RequestMethod.GET)
    public String deleteFromBasket(@PathVariable(value = "id") int itemId, Model model) {
        int result = basketService.removeItemByProductId(itemId);
        if (result > 0) {
            model.addAttribute("message", "Товар удален из корзины");
        } else {
            model.addAttribute("message", "Ошибка при удалении их корзины. Обратитесь к администратору.");
        }
        return showBasket(model);
    }

    @RequestMapping(value = "/basket/clear")
    public String clearBasket(Model model) {
        int result = basketService.clearBasket();
        if (result < 1) {
            model.addAttribute("message", "Ошибка при удалении из корзины. Обратитесь к администратору");
        }
        return showBasket(model);
    }
}
