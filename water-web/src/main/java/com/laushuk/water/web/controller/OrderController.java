package com.laushuk.water.web.controller;

import com.laushuk.water.service.BasketService;
import com.laushuk.water.service.OrderService;
import com.laushuk.water.service.UserService;
import com.laushuk.water.service.model.OrderDTO;
import com.laushuk.water.service.model.OrderSubmitDTO;
import com.laushuk.water.service.model.UserDTOFull;
import com.laushuk.water.web.controller.validator.OrderValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Controller
@SessionAttributes({"userFull", "oldOrderDTO"})
public class OrderController {

    private UserService userService;
    private OrderService orderService;
    private BasketService basketService;
    private OrderValidator orderValidator;

    @Autowired
    public OrderController(UserService userService, OrderService orderService, BasketService basketService, OrderValidator orderValidator) {
        this.userService = userService;
        this.orderService = orderService;
        this.basketService = basketService;
        this.orderValidator = orderValidator;
    }

    @RequestMapping(value = "/user/order/add", method = RequestMethod.GET)
    public String showOrderForm(Model model, @ModelAttribute("orderSubmitDTO") OrderSubmitDTO orderSubmitDTO) {
        UserDTOFull userFull = userService.get();
        model.addAttribute("userFull", userFull);
        return "user/add-order";
    }

    @RequestMapping(value = "/user/order/add", method = RequestMethod.POST)
    public String addOrder(
            @ModelAttribute("orderSubmitDTO") OrderSubmitDTO orderSubmitDTO,
            Model model,
            final RedirectAttributes redirectAttributes,
            BindingResult result,
            SessionStatus status
    ) {
        orderValidator.validate(orderSubmitDTO, result);
        if (!result.hasErrors()) {
            int r = orderService.saveOrUpdate(orderSubmitDTO);
            if (r < 1) {
                redirectAttributes.addFlashAttribute("message", "Ошибка. Заказ не сохранен");
                return showOrderForm(model, orderSubmitDTO);
            } else {
                status.setComplete();
                redirectAttributes.addFlashAttribute("message", "Заказ сохранен");
                return "redirect:/user/shop";
            }
        } else return "user/add-order";
    }


    @RequestMapping(value = "/user/orders", method = RequestMethod.GET)
    public String showMyOrders(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            Model model
    ) {
        int itemsPerPage = 10;
        List<OrderDTO> orders = orderService.getPaginated(page, itemsPerPage);
        model.addAttribute("ordersList", orders);
        Long pagesQuantity = orderService.getPagesQuantity(itemsPerPage);
        model.addAttribute("pagesQuantity", pagesQuantity);
        return "user/orders-list";
    }


    @RequestMapping(value = "/admin/orders", method = RequestMethod.GET)
    public String showOrders(
            @RequestParam(value = "page", required = false, defaultValue = "1") int page,
            Model model
    ) {
        int itemsPerPage = 10;
        String city = null;
        List<OrderDTO> orders = orderService.getPaginated(page, city, itemsPerPage);
        model.addAttribute("ordersList", orders);
        Long pagesQuantity = orderService.getPagesQuantity(city, itemsPerPage);
        model.addAttribute("pagesQuantity", pagesQuantity);
        return "admin/orders-list";
    }


    @RequestMapping(value = "/user/order/edit", method = RequestMethod.POST)
    public String editOrder(
            @RequestParam("orderId") int orderId,
            Model model,
            final RedirectAttributes redirectAttributes
    ) {
        basketService.clearBasket();
        OrderDTO oldOrderDTO = orderService.putIntoBasketById(orderId);
        if (oldOrderDTO == null) {
            redirectAttributes.addFlashAttribute
                    ("message", "Ошибка при попытке изменения заказа. Попробуйте удалить его и создать новый");
            return "redirect:/user/order/show/" + orderId;
        } else {
            model.addAttribute("oldOrderDTO", oldOrderDTO);
            return "redirect:/user/basket";
        }
    }


    @RequestMapping(value = "/user/order/delete", method = RequestMethod.POST)
    public String deleteOrder(@RequestParam("orderId") int orderId,
                              final RedirectAttributes redirectAttributes) {
        basketService.clearBasket();
        int result = orderService.removeById(orderId);
        if (result < 0) {
            redirectAttributes.addFlashAttribute
                    ("message", "Ошибка при попытке удаления заказа. Обратитесь к администратору");
        } else {
            redirectAttributes.addFlashAttribute("message", "Заказ удален");

        }
        return "redirect:/user/orders";
    }


    @RequestMapping(value = "/user/order/show/{orderId}", method = RequestMethod.GET)
    public String showOrder (@PathVariable("orderId") int orderId, Model model) {
        OrderDTO orderDTO = orderService.getById(orderId);
        model.addAttribute("orderDTO", orderDTO);
        return "user/order";
    }


    @RequestMapping(value = "/admin/order/show/{orderId}", method = RequestMethod.GET)
    public String showOrderAdmin (@PathVariable("orderId") int orderId, Model model) {
        OrderDTO orderDTO = orderService.getById(orderId);
        model.addAttribute("orderDTO", orderDTO);
        return "admin/order";
    }

    @RequestMapping(value = "/admin/order/show/{orderId}", method = RequestMethod.POST)
    public String changeOrderStatus(@PathVariable("orderId") int orderId,
                                    @RequestParam("status") String status,
                                    Model model) {
        int result = orderService.updateStatus(orderId, status);
        if (result > 0) model.addAttribute("message", "Статус обновлен");
        else model.addAttribute("message", "Ошибка. Статус не обновлен");
        return showOrderAdmin(orderId, model);
    }
}
