package com.laushuk.water.web.controller.validator;

import com.laushuk.water.service.UserService;
import com.laushuk.water.service.model.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Component
public class UserValidator implements Validator{
    private final UserService userService;

    @Autowired
    public UserValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UserDTO.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDTO userDTO = (UserDTO) o;
        if (!userDTO.getPassword().equals(userDTO.getPassrepeat())) {
            errors.rejectValue("password", "error.passwords.don't.match", "o-ho-ho");
        }
        if (userService.isExist(userDTO.getLogin())){
            errors.rejectValue("login", "error.login.exist","u-la-la");
        }
        if (userDTO.getCity()==null) {
            errors.rejectValue("city", "error.city.not.selected", "please, select the city");
        }
    }
}
