package com.laushuk.water.web.controller.exception;

import org.apache.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@ControllerAdvice
public class ExceptionController {

    private static final Logger log = Logger.getLogger(ExceptionController.class);

    @ExceptionHandler(value = RuntimeException.class)
    public String processRuntimeException(RuntimeException e){
        log.error(e.getMessage(), e);
        return "redirect:/login";
    }
}
