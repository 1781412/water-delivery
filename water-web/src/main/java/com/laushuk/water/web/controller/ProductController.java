package com.laushuk.water.web.controller;

import com.laushuk.water.service.ProductService;
import com.laushuk.water.service.model.ProductDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Controller
public class ProductController {

    private static final Logger log = Logger.getLogger(ProductController.class);

    private final ProductService productService;

    @Autowired
    public ProductController(ProductService productService) {
        this.productService = productService;
    }

    @RequestMapping(value = "/admin/product/add", method = RequestMethod.GET)
    public String showProductAddForm(@ModelAttribute("product") ProductDTO product) {
        return "admin/add-product";
    }

    @RequestMapping(value = "/admin/product/add", method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute("product") ProductDTO product) throws IOException {
        productService.save(product);
        return "redirect:/admin/products?page=1";
    }

    @RequestMapping(value = "/admin/products", method = RequestMethod.GET)
    public String showProductsList(@RequestParam("page") int page, Model model) {
        int postPerPage = 10;
        List<ProductDTO> products = productService.getPaginatedItems(page, 0, postPerPage);
        Long pagesQuantity = productService.getPagesQuantity(0, postPerPage);
        model.addAttribute("productsList", products);
        model.addAttribute("pagesQuantity", pagesQuantity);
        return "admin/products-list";
    }

    @RequestMapping(value = "/admin/product/{id}", method = RequestMethod.GET)
    public String showProduct(@PathVariable("id") int id, Model model) {
        ProductDTO productDTO = productService.get(id);
        model.addAttribute("product", productDTO);
        return "admin/product";
    }

    @RequestMapping(value = "/admin/product/status-change/{id}", method = RequestMethod.POST)
    public String changeStatus(
            @PathVariable("id") int id,
            @RequestParam("status") boolean status
    ) {
        productService.updateStatus(id, status);
        return "redirect:/admin/product/" + id;
    }

    @RequestMapping(value = "/admin/product/copy/{id}", method = RequestMethod.POST)
    public String copyProduct (@PathVariable ("id") int id, Model model) {
        ProductDTO productDTO = productService.copy(id);
        model.addAttribute("product", productDTO);
        return "admin/add-product";
    }

    @RequestMapping(value = "/admin/product/update", method = RequestMethod.POST)
    public String editProduct(@RequestParam("id") int id, Model model) {
        ProductDTO productDTO = productService.get(id);
        model.addAttribute("product", productDTO);
        return "admin/add-product";
    }
}
