package com.laushuk.water.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Entity
@Table(name = "T_BASKET")
@NamedQueries({
        @NamedQuery(name = "HQL_REMOVE_PRODUCT_BY_ID",
                query = "delete from Basket where orderId = null and userId = ? and product.id = ?"),
        @NamedQuery(name = "HQL_CLEAR_BASKET",
                query = "delete from Basket where orderId = null and userId = ?")
})
public class Basket implements Serializable {
    private static final long serialVersionUID = -4012740743905361920L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;
    @Column(name = "F_USER_ID")
    private Integer userId;
    @Column(name = "F_ORDER_ID")
    private Integer orderId;
    @ManyToOne
    @JoinColumn(name = "F_PRODUCT_ID", nullable = false)
    private Product product;
    @Column(name = "F_QUANTITY")
    private Integer quantity;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Basket basket = (Basket) o;
        return Objects.equals(id, basket.id) &&
                Objects.equals(userId, basket.userId) &&
                Objects.equals(orderId, basket.orderId) &&
                Objects.equals(product, basket.product) &&
                Objects.equals(quantity, basket.quantity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userId, orderId, product, quantity);
    }

    @Override
    public String toString() {
        return "Basket{" +
                "id=" + id +
                ", userId=" + userId +
                ", orderId=" + orderId +
                ", productId=" + product +
                ", quantity=" + quantity +
                '}';
    }
}
