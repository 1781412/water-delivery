package com.laushuk.water.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Entity
@Table(name = "T_NEWS")
@NamedQueries({
        @NamedQuery(name = "HQL_GET_FILTERED_NEWS",
                query = "from News where visible = ? order by F_ID desc "),
        @NamedQuery(name = "HQL_GET_ALL_NEWS",
                query = "from News order by F_ID desc "),
        @NamedQuery(name = "HQL_COUNT_FILTERED_NEWS",
                query = "select count (*) from News where visible = ?"),
        @NamedQuery(name = "HQL_COUNT_ALL_NEWS",
                query = "select count (*) from News"),
        @NamedQuery(name = "HQL_PUBLISH",
                query = "update News set visible = true where id = ?")
})
public class News implements Serializable {
    private static final long serialVersionUID = -5613470380172374249L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;
    @Column(name = "F_HEADLINE")
    private String headline;
    @Column(name = "F_NEWS_TEXT", length = 2000)
    private String newsText;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "F_FILE_ID")
    private FileEntity postFileEntity;
    @Column(name = "F_VISIBLE")
    private Boolean visible;
    @Column(name = "F_PUBLICATION_DATE")
    private Date publicationDate;
    @ManyToOne
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeadline() {
        return headline;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
    }

    public String getNewsText() {
        return newsText;
    }

    public void setNewsText(String newsText) {
        this.newsText = newsText;
    }

    public FileEntity getPostFileEntity() {
        return postFileEntity;
    }

    public void setPostFileEntity(FileEntity postFileEntity) {
        this.postFileEntity = postFileEntity;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        News news = (News) o;
        return id == news.id &&
                visible == news.visible &&
                Objects.equals(headline, news.headline) &&
                Objects.equals(newsText, news.newsText) &&
                Objects.equals(postFileEntity, news.postFileEntity) &&
                Objects.equals(publicationDate, news.publicationDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, headline, newsText, postFileEntity, visible, publicationDate, user);
    }

    @Override
    public String toString() {
        return "News{" +
                "id=" + id +
                ", headline='" + headline + '\'' +
                ", newsText='" + newsText + '\'' +
                ", postFileEntity='" + postFileEntity + '\'' +
                ", visible=" + visible +
                ", publicationDate='" + publicationDate + '\'' +
                ", userId=" + user.getId() +
                '}';
    }
}
