package com.laushuk.water.repository.model;

import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Entity
@Table(name = "T_USER")
public class User implements Serializable {
    private static final long serialVersionUID = -3496178868003376124L;

    @Id
    @Column(name = "F_ID")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "F_EMAIL")
    private String login;
    @Column(name = "F_PASSWORD")
    private String password;
    @Column(name = "F_ROLE")
    @Enumerated(EnumType.STRING)
    private UserRole role;
    @Column(name = "F_USER_STATUS")
    @Enumerated(EnumType.STRING)
    private UserStatus userStatus;
    @Column(name = "F_NAME")
    private String name;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "F_USER_ID")
    private Set<UserAddress> addresses = new HashSet<>();
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "F_USER_ID")
    private Set<UserPhone> phones = new HashSet<>();
    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    private UserInformation userInformation;
    @OneToMany(mappedBy = "user")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Set<News> news;
    @OneToMany(mappedBy = "user")
    @Cascade(org.hibernate.annotations.CascadeType.SAVE_UPDATE)
    private Set<Order> orders;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public UserStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatus userStatus) {
        this.userStatus = userStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<UserAddress> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<UserAddress> addresses) {
        this.addresses = addresses;
    }

    public Set<UserPhone> getPhones() {
        return phones;
    }

    public void setPhones(Set<UserPhone> phones) {
        this.phones = phones;
    }

    public UserInformation getUserInformation() {
        return userInformation;
    }

    public void setUserInformation(UserInformation userInformation) {
        this.userInformation = userInformation;
    }

    public Set<News> getNews() {
        return news;
    }

    public void setNews(Set<News> news) {
        this.news = news;
    }

    public Set<Order> getOrders() {
        return orders;
    }

    public void setOrders(Set<Order> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(login, user.login) &&
                Objects.equals(password, user.password) &&
                role == user.role &&
                userStatus == user.userStatus &&
                Objects.equals(name, user.name) &&
                Objects.equals(addresses, user.addresses) &&
                Objects.equals(phones, user.phones) &&
                Objects.equals(userInformation, user.userInformation);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, login, password, role, userStatus, name, addresses, phones, userInformation);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", login='" + login + '\'' +
                ", password='" + password + '\'' +
                ", role=" + role +
                ", userStatus=" + userStatus +
                ", name='" + name + '\'' +
                '}';
    }
}
