package com.laushuk.water.repository;

import com.laushuk.water.repository.model.Message;
import com.laushuk.water.repository.model.Order;

import java.util.List;

/**
 * Created by p.laushuk on 01.09.2017. Email:1781412@gmail.com
 */
public interface MessageDao extends GenericDao<Message, Integer> {
    List<Message> getPaginated(int page, int itemsPerPage);

    Long getPostsQuantity();
}
