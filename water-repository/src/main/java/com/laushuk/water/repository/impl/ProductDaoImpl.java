package com.laushuk.water.repository.impl;

import com.laushuk.water.repository.ProductDao;
import com.laushuk.water.repository.model.Basket;
import com.laushuk.water.repository.model.Product;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Repository
public class ProductDaoImpl extends GenericDaoImpl<Product, Integer> implements ProductDao {

    @Override
    public List<Product> getPaginatedItems(Integer page, Integer available, Integer itemsPerPage) {
        Criteria criteria = getSession().createCriteria(Product.class);
        criteria.addOrder(Order.desc("id"));
        if (available > 0) {
            criteria.add(Restrictions.eq("available", true));
        }
        if (available < 0) {
            criteria.add(Restrictions.eq("available", false));
        }
        criteria.setFirstResult((page - 1) * itemsPerPage);
        criteria.setMaxResults(itemsPerPage);
        return criteria.list();
    }

    @Override
    public Long getRowsQuantity(Integer available) {
        Criteria criteria = getSession().createCriteria(Product.class);
        criteria.setProjection(Projections.rowCount());
        if (available > 0) {
            criteria.add(Restrictions.eq("available", true));
        }
        if (available < 0) {
            criteria.add(Restrictions.eq("available", false));
        }
        return (Long) criteria.uniqueResult();
    }

    @Override
    public Long isUsed(Integer id) {
        Criteria criteria = getSession().createCriteria(Basket.class);
        criteria.setProjection(Projections.rowCount());
        criteria.add(Restrictions.eq("product.id", id));
        return (Long) criteria.uniqueResult();
    }
}
