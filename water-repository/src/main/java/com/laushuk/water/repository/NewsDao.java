package com.laushuk.water.repository;

import com.laushuk.water.repository.model.News;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface NewsDao extends GenericDao<News, Integer> {

    List<News> getPaginatedPosts(Integer page, Integer visible, Integer postPerPage);

    Long getPostsQuantity(Integer visible);

    int publish(Integer id);
}
