package com.laushuk.water.repository;

import com.laushuk.water.repository.model.User;
import com.laushuk.water.repository.model.UserRole;
import com.laushuk.water.repository.model.UserStatus;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface UserDao extends GenericDao<User, Integer> {

    boolean isExist(String login);

    User getByLogin(String login);

    List<User> getPaginatedUsers(Integer page, UserStatus status, UserRole role, Integer itemsPerPage);

    Long getItemQuantity(UserStatus status, UserRole role);

}
