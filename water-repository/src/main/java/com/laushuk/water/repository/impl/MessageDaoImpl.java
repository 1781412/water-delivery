package com.laushuk.water.repository.impl;

import com.laushuk.water.repository.MessageDao;
import com.laushuk.water.repository.OrderDao;
import com.laushuk.water.repository.model.Message;
import com.laushuk.water.repository.model.Order;
import com.laushuk.water.repository.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Repository
public class MessageDaoImpl extends GenericDaoImpl<Message, Integer> implements MessageDao {


    @Override
    public List<Message> getPaginated(int page, int itemsPerPage) {
        Criteria criteria = getSession().createCriteria(Message.class);
        criteria.addOrder(org.hibernate.criterion.Order.asc("date"));
        criteria.setFirstResult((page - 1) * itemsPerPage);
        criteria.setMaxResults(itemsPerPage);
        return criteria.list();
    }

    @Override
    public Long getPostsQuantity() {
        Criteria criteria = getSession().createCriteria(Message.class);
        criteria.setProjection(Projections.rowCount());
        return (Long) criteria.uniqueResult();
    }

}
