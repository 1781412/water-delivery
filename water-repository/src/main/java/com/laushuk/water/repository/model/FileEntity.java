package com.laushuk.water.repository.model;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Entity
@Table(name = "T_FILE")
public class FileEntity implements Serializable {
    private static final long serialVersionUID = -3931962834353262693L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer postId;
    @Column(name = "F_FILE_NAME")
    private String fileName;
    @Column(name = "F_LOCATION")
    private String location;

    public Integer getPostId() {
        return postId;
    }

    public void setPostId(Integer postId) {
        this.postId = postId;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    /*public News getPost() {
        return post;
    }

    public void setPost(News post) {
        this.post = post;
    }*/
}
