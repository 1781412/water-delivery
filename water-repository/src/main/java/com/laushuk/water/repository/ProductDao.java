package com.laushuk.water.repository;

import com.laushuk.water.repository.model.Product;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface ProductDao extends GenericDao<Product, Integer> {

    List<Product> getPaginatedItems(Integer page, Integer available, Integer itemsPerPage);

    Long getRowsQuantity(Integer available);

    Long isUsed(Integer id);
}
