package com.laushuk.water.repository.impl;

import com.laushuk.water.repository.FileDao;
import com.laushuk.water.repository.model.FileEntity;
import org.springframework.stereotype.Repository;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Repository
public class FileDaoImpl extends GenericDaoImpl<FileEntity, Integer> implements FileDao {

}
