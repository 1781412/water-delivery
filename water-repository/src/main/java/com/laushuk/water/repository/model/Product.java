package com.laushuk.water.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Entity
@Table(name = "T_PRODUCT")
public class Product implements Serializable {
    private static final long serialVersionUID = -1417798650909923921L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;
    @Column(name = "F_INV_NUM")
    private String invNum;
    @Column(name = "F_NAME")
    private String name;
    @Column(name = "F_DESCRIPTION")
    private String description;
    @Column(name = "F_PRICE")
    private BigDecimal price;
    @Column(name = "F_AVAILABLE")
    private boolean available;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "F_FILE_ID")
    private FileEntity fileEntity;
    @Column(name = "F_CREATION_DATE")
    private Date creationDate;
    @ManyToOne
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getInvNum() {
        return invNum;
    }

    public void setInvNum(String invNum) {
        this.invNum = invNum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public boolean getAvailable() {
        return available;
    }

    public void setAvailable(boolean available) {
        this.available = available;
    }

    public FileEntity getFileEntity() {
        return fileEntity;
    }

    public void setFileEntity(FileEntity fileEntity) {
        this.fileEntity = fileEntity;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return available == product.available &&
                Objects.equals(id, product.id) &&
                Objects.equals(invNum, product.invNum) &&
                Objects.equals(name, product.name) &&
                Objects.equals(description, product.description) &&
                Objects.equals(price, product.price) &&
                Objects.equals(fileEntity, product.fileEntity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, invNum, name, description, price, available, fileEntity);
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", invNum='" + invNum + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", available=" + available +
                ", imagePath='" + fileEntity + '\'' +
                '}';
    }
}
