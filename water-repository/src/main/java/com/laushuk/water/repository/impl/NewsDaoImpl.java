package com.laushuk.water.repository.impl;

import com.laushuk.water.repository.NewsDao;
import com.laushuk.water.repository.model.News;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Repository
public class NewsDaoImpl extends GenericDaoImpl<News, Integer> implements NewsDao {

    @Override
    public List<News> getPaginatedPosts(Integer page, Integer visible, Integer postPerPage) {
        int maxResults = postPerPage;
        Query query = getSession().getNamedQuery("HQL_GET_FILTERED_NEWS");
        if (visible == 0) {
            query = getSession().getNamedQuery("HQL_GET_ALL_NEWS");
        } else if (visible > 0) {
            query.setParameter(0, true);
        } else {
            query.setParameter(0, false);
        }
        query.setFirstResult((page - 1) * maxResults);
        query.setMaxResults(maxResults);
        return query.list();
    }

    @Override
    public Long getPostsQuantity(Integer visible) {
        Query query = getSession().getNamedQuery("HQL_COUNT_FILTERED_NEWS");
        if (visible == 0) {
            query = getSession().getNamedQuery("HQL_COUNT_ALL_NEWS");
        } else if (visible > 0) {
            query.setParameter(0, true);
        } else {
            query.setParameter(0, false);
        }
        return (Long) query.uniqueResult();
    }

    @Override
    public int publish(Integer id) {
        Query query = getSession().getNamedQuery("HQL_PUBLISH");
        query.setParameter(0, id);
        return query.executeUpdate();
    }
}
