package com.laushuk.water.repository;

import com.laushuk.water.repository.model.FileEntity;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface FileDao extends GenericDao<FileEntity, Integer> {
}
