package com.laushuk.water.repository.impl;

import com.laushuk.water.repository.BasketDao;
import com.laushuk.water.repository.model.Basket;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Repository
public class BasketDaoImpl extends GenericDaoImpl<Basket, Integer> implements BasketDao {

    @Override
    public List<Basket> getByUserId(Integer userId, Integer orderId) {
        Criteria criteria = getSession().createCriteria(Basket.class);
        criteria.add(Restrictions.eq("userId", userId));
        if (orderId != null) {
            criteria.add(Restrictions.eq("orderId", orderId));
        } else {
            criteria.add(Restrictions.isNull("orderId"));
        }
        return criteria.list();
    }

    @Override
    public int removeProductById(Integer userId, Integer product_id) {
        Query query = getSession().getNamedQuery("HQL_REMOVE_PRODUCT_BY_ID");
        query.setParameter(0, userId);
        query.setParameter(1, product_id);
        return query.executeUpdate();
    }

    @Override
    public int clearBasket(Integer userId) {
        Query query = getSession().getNamedQuery("HQL_CLEAR_BASKET");
        query.setParameter(0, userId);
        return query.executeUpdate();
    }
}
