package com.laushuk.water.repository.impl;

import com.laushuk.water.repository.GenericDao;
import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@SuppressWarnings("unchecked")
@Repository
public abstract class GenericDaoImpl<T extends Serializable, ID extends Serializable> implements GenericDao<T, ID> {
    private static final Logger log = Logger.getLogger(GenericDaoImpl.class);

    private final Class<T> entityClass;

    @Autowired
    private SessionFactory sessionFactory;

    public GenericDaoImpl() {
        this.entityClass = (Class<T>) ((ParameterizedType) this.getClass().getGenericSuperclass())
                .getActualTypeArguments()[0];
    }

    protected Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    @Override
    public T findById(final ID id) {
        log.debug("Starting to getItem with Hibernate");
        return (T) getSession().get(this.entityClass, id);
    }

    @Override
    public void saveOrUpdate(T item) {
        log.debug("Starting to saveOrUpdate with Hibernate");
        getSession().saveOrUpdate(item);
    }

    @Override
    public void delete(T item) {
        log.debug("Starting to delete with Hibernate");
        getSession().delete(item);
    }

    @Override
    public void save(T item) {
        log.debug("Starting to save with Hibernate");
        getSession().save(item);
    }

}
