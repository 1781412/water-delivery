package com.laushuk.water.repository;

import com.laushuk.water.repository.model.Basket;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface BasketDao extends GenericDao<Basket, Integer> {

    List<Basket> getByUserId(Integer userId, Integer orderId);

    int removeProductById(Integer userId, Integer product_id);

    int clearBasket(Integer userId);
}
