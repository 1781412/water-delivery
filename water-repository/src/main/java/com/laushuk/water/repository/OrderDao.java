package com.laushuk.water.repository;

import com.laushuk.water.repository.model.Order;
import com.laushuk.water.repository.model.User;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface OrderDao extends GenericDao<Order, Integer> {

    List<Order> getPaginatedItems(int page, String city, int itemsPerPage);

    List<Order> getPaginatedItems(int page, User user, int itemsPerPage);

    Long getPostsQuantity(String city);

    Long getPostsQuantity(User user);

    void deleteById(Integer orderId);
}
