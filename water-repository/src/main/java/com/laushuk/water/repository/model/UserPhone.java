package com.laushuk.water.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Entity
@Table(name = "T_USER_PHONE")
public class UserPhone implements Serializable {
    private static final long serialVersionUID = -3914467710730457538L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;
    @Column(name = "F_PHONE")
    private String phone;
    @Column(name = "F_USER_ID")
    private Integer userId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserPhone userPhone = (UserPhone) o;
        return Objects.equals(id, userPhone.id) &&
                Objects.equals(phone, userPhone.phone) &&
                Objects.equals(userId, userPhone.userId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, phone, userId);
    }

    @Override
    public String toString() {
        return "UserPhone{" +
                "id=" + id +
                ", phone='" + phone + '\'' +
                ", userId=" + userId +
                '}';
    }
}
