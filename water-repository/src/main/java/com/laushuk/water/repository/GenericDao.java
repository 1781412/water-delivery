package com.laushuk.water.repository;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface GenericDao<T, ID> {

    T findById(final ID id);

    void saveOrUpdate(T item);

    void delete(T item);

    void save(T item);
}
