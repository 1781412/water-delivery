package com.laushuk.water.repository.impl;

import com.laushuk.water.repository.UserDao;
import com.laushuk.water.repository.model.User;
import com.laushuk.water.repository.model.UserRole;
import com.laushuk.water.repository.model.UserStatus;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Properties;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Repository
public class UserDaoImpl extends GenericDaoImpl<User, Integer> implements UserDao {
    private static Logger log = Logger.getLogger(UserDaoImpl.class);
    private Properties properties;

    public UserDaoImpl(Properties properties) {
        this.properties = properties;
    }

    @Override
    public boolean isExist(String login) {

        Query query = getSession().createQuery(properties.getProperty("query.hql.user.isExists"));
        query.setParameter(0, login);
        Long count = (Long) query.uniqueResult();

        return count > 0;
    }

    @Override
    public User getByLogin(String login) {
        Query query = getSession().createQuery(properties.getProperty("query.hql.user.getByLogin"));
        query.setParameter(0, login);
        List<User> users = query.list();
        if (!users.isEmpty()) {
            return users.get(0);
        } else return null;
    }

    public List<User> getPaginatedUsers(Integer page, UserStatus status, UserRole role, Integer itemsPerPage) {
        String q = (properties.getProperty("query.hql.user.getPaginated"));
        if (status != null && role != null) {
            q += properties.getProperty("query.hql.user.getPaginated.filterStatusAndRole");
        } else if (status != null) {
            q += properties.getProperty("query.hql.user.getPaginated.filterStatus");
        } else if (role != null) {
            q += properties.getProperty("query.hql.user.getPaginated.filterRole");
        }
        q += properties.getProperty("query.hql.user.getPaginated.order");

        Query query = getSession().createQuery(q);
        if (status != null) {
            query.setParameter("status", status);
        }
        if (role != null) {
            query.setParameter("role", role);
        }
        query.setFirstResult((page - 1) * itemsPerPage);
        query.setMaxResults(itemsPerPage);
        return query.list();
    }

    public Long getItemQuantity(UserStatus status, UserRole role) {
        StringBuilder q = new StringBuilder(properties.getProperty("query.hql.user.getItemQuantity"));
        if (status != null && role != null) {
            q.append(properties.getProperty("query.hql.user.getPaginated.filterStatusAndRole"));
        } else if (status != null) {
            q.append(properties.getProperty("query.hql.user.getPaginated.filterStatus"));
        } else if (role != null) {
            q.append(properties.getProperty("query.hql.user.getPaginated.filterRole"));
        }

        Query query = getSession().createQuery(q.toString());
        if (status != null) {
            query.setParameter("status", status);
        }
        if (role != null) {
            query.setParameter("role", role);
        }
        return (Long) query.uniqueResult();
    }
}
