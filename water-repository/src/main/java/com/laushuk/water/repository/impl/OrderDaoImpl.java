package com.laushuk.water.repository.impl;

import com.laushuk.water.repository.OrderDao;
import com.laushuk.water.repository.model.Order;
import com.laushuk.water.repository.model.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Repository
public class OrderDaoImpl extends GenericDaoImpl<Order, Integer> implements OrderDao {


    @Override
    public List<Order> getPaginatedItems(int page, String city, int itemsPerPage) {
        Criteria criteria = getSession().createCriteria(Order.class);
        criteria.addOrder(org.hibernate.criterion.Order.asc("deliveryDate"));
        if (city != null) {
            criteria.add(Restrictions.eq("deliveryAddress.city", city));
        }
        criteria.setFirstResult((page - 1) * itemsPerPage);
        criteria.setMaxResults(itemsPerPage);
        return criteria.list();
    }

    @Override
    public List<Order> getPaginatedItems(int page, User user, int itemsPerPage) {
        Criteria criteria = getSession().createCriteria(Order.class);
        criteria.addOrder(org.hibernate.criterion.Order.asc("deliveryDate"));
        criteria.add(Restrictions.eq("user", user));
        criteria.setFirstResult((page - 1) * itemsPerPage);
        criteria.setMaxResults(itemsPerPage);
        return criteria.list();
    }

    @Override
    public Long getPostsQuantity(String city) {
        Criteria criteria = getSession().createCriteria(Order.class);
        criteria.setProjection(Projections.rowCount());
        if (city != null) {
            criteria.add(Restrictions.eq("deliveryAddress.city", city));
        }
        return (Long) criteria.uniqueResult();
    }

    @Override
    public Long getPostsQuantity(User user) {
        Criteria criteria = getSession().createCriteria(Order.class);
        criteria.setProjection(Projections.rowCount());
        criteria.add(Restrictions.eq("user", user));
        return (Long) criteria.uniqueResult();
    }

    @Override
    public void deleteById(Integer orderId) {
        Order o = getSession().get(Order.class, orderId);
        getSession().delete(o);
    }
}
