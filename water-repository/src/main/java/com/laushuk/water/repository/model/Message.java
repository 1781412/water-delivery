package com.laushuk.water.repository.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Entity
@Table(name = "T_MESSAGE")
public class Message implements Serializable {
    private static final long serialVersionUID = 5613470354682374249L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;
    @Column(name = "F_EMAIL")
    private String email;
    @Column(name = "F_SUBJECT")
    private String subject;
    @Column(name = "F_MESSAGE", length = 2000)
    private String message;
    @Column(name = "F_DATE")
    private Date date;
    @ManyToOne
    @JoinColumn(name = "F_USER_ID")
    private User user;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message1 = (Message) o;
        return Objects.equals(getId(), message1.getId()) &&
                Objects.equals(getEmail(), message1.getEmail()) &&
                Objects.equals(getSubject(), message1.getSubject()) &&
                Objects.equals(getMessage(), message1.getMessage()) &&
                Objects.equals(getDate(), message1.getDate()) &&
                Objects.equals(getUser(), message1.getUser());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getEmail(), getSubject(), getMessage(), getDate(), getUser());
    }
}
