package com.laushuk.water.repository.model;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public enum UserRole {
    ROLE_USER, ROLE_ADMIN, ROLE_SUPERADMIN,
    INVALID;
}
