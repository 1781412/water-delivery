package com.laushuk.water.repository.model;

import org.hibernate.annotations.*;
import org.hibernate.annotations.CascadeType;

import javax.persistence.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Entity
@Table(name = "T_ORDER")
public class Order implements Serializable {
    private static final long serialVersionUID = 4158596247706595780L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "F_ID")
    private Integer id;             //required
    @ManyToOne
    @JoinColumn(name = "F_USER_ID", nullable = false)
    private User user;
    @Column(name = "F_STATUS")
    @Enumerated(EnumType.STRING)
    private OrderStatus status;     //required
    @Column(name = "F_CREATION_DATE")
    private Date creationDate;
    @Column(name = "F_DELIVERY_DATE")
    private Date deliveryDate;
    @Column(name = "F_DELIVERY_TIME")
    @Enumerated(EnumType.STRING)
    private OrderDeliveryTime deliveryTime;
    @ManyToOne
    @JoinColumn(name = "F_DELIVERY_ADDRESS")
    @Cascade(CascadeType.SAVE_UPDATE)
    private UserAddress deliveryAddress;
    @ManyToOne
    @JoinColumn(name = "F_DELIVERY_PHONE")
    @Cascade(CascadeType.SAVE_UPDATE)
    private UserPhone deliveryPhone;
    @Column(name = "F_DELIVERY_INFORMATION")
    private String deliveryInformation;
    @OneToMany(orphanRemoval = true)
    @JoinColumn(name = "F_ORDER_ID")
    private Set<Basket> basket = new HashSet<>();
    @Column(name = "F_COST")
    private BigDecimal cost;        //required

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public OrderDeliveryTime getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(OrderDeliveryTime deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public UserAddress getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(UserAddress deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public UserPhone getDeliveryPhone() {
        return deliveryPhone;
    }

    public void setDeliveryPhone(UserPhone deliveryPhone) {
        this.deliveryPhone = deliveryPhone;
    }

    public String getDeliveryInformation() {
        return deliveryInformation;
    }

    public void setDeliveryInformation(String deliveryInformation) {
        this.deliveryInformation = deliveryInformation;
    }

    public Set<Basket> getBasket() {
        return basket;
    }

    public void setBasket(Set<Basket> basket) {
        this.basket = basket;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return Objects.equals(id, order.id) &&
                Objects.equals(user, order.user) &&
                status == order.status &&
                Objects.equals(deliveryDate, order.deliveryDate) &&
                deliveryTime == order.deliveryTime &&
                Objects.equals(deliveryAddress, order.deliveryAddress) &&
                Objects.equals(deliveryPhone, order.deliveryPhone) &&
                Objects.equals(deliveryInformation, order.deliveryInformation) &&
                Objects.equals(basket, order.basket) &&
                Objects.equals(cost, order.cost);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, user, status, deliveryDate, deliveryTime, deliveryAddress, deliveryPhone, deliveryInformation, basket, cost);
    }

    @Override
    public String toString() {
        return "Order{" +
                "id=" + id +
                ", user=" + user +
                ", status=" + status +
                ", creationDate=" + creationDate +
                ", deliveryDate=" + deliveryDate +
                ", deliveryTime=" + deliveryTime +
                ", deliveryAddress=" + deliveryAddress +
                ", deliveryPhone=" + deliveryPhone +
                ", deliveryInformation='" + deliveryInformation + '\'' +
                ", basket=" + basket +
                ", cost=" + cost +
                '}';
    }
}
