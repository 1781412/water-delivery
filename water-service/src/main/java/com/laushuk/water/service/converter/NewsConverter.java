package com.laushuk.water.service.converter;

import com.laushuk.water.repository.model.News;
import com.laushuk.water.service.model.PostDTO;
import org.apache.log4j.Logger;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class NewsConverter {
    private static final Logger logger = Logger.getLogger(NewsConverter.class);

    private NewsConverter() {
    }

    public static News setPost(News post, PostDTO postDTO) {
        post.setId(postDTO.getId());
        post.setHeadline(postDTO.getHeadline());
        post.setNewsText(postDTO.getNewsText());
        /*post.setImagePath(postDTO.getImagePath());*/
        post.setVisible(postDTO.isVisible());
        post.setPublicationDate(postDTO.getPublicationDate());

        return post;
    }

    public static PostDTO setPostDTO(News post) {
        PostDTO postDTO = new PostDTO();
        postDTO.setId(post.getId());
        /*postDTO.setUserName(post.getUser().getName());*/
        postDTO.setUserDTO(UserConverter.setUserDTO(post.getUser()));
        postDTO.setHeadline(post.getHeadline());
        postDTO.setNewsText(post.getNewsText());
        postDTO.setVisible(post.getVisible());
        postDTO.setPublicationDate(post.getPublicationDate());
        if (post.getPostFileEntity() != null) {
            postDTO.setImageId(post.getPostFileEntity().getPostId());
        }
        return postDTO;
    }

}
