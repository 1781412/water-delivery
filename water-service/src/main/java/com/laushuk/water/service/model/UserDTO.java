package com.laushuk.water.service.model;

import com.laushuk.water.repository.model.User;

public class UserDTO {
    private Integer id;
    private String name;
    private String lastName;
    private String middleName;
    private String additionalInfo;
    private UserRoleDTO role;
    private UserStatusDTO userStatus;
    private String address;
    private String city;
    private String login;
    private String password;
    private String passrepeat;
    private String phone;

    public UserDTO() {
    }

    public UserDTO(User user) {
        this(
                newBuilder()
                        //додедать новые поля
                        .id(user.getId())
                        .name(user.getName())
                        /*.lastName(user.getUserInformation().getLastName())*/
                        .role(UserRoleDTO.valueOf(user.getRole().toString()))
                        .userStatus(UserStatusDTO.valueOf(user.getUserStatus().toString()))
                        /*.address(user.getAddress())*/
                        /*.city(user.getCity())*/
                        .login(user.getLogin())
                        /*.password(user.getPassword())*/
                        /*.phone(user.getPhone())*/
        );
    }

    private UserDTO(Builder builder) {
        id = builder.id;
        setName(builder.name);
        setLastName(builder.lastName);
        setMiddleName(builder.middleName);
        setAdditionalInfo(builder.additionalInfo);
        setRole(builder.role);
        setUserStatus(builder.userStatus);
        setAddress(builder.address);
        setCity(builder.city);
        setLogin(builder.login);
        setPassword(builder.password);
        setPhone(builder.phone);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UserRoleDTO getRole() {
        return role;
    }

    public void setRole(UserRoleDTO role) {
        this.role = role;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassrepeat() {
        return passrepeat;
    }

    public void setPassrepeat(String passrepeat) {
        this.passrepeat = passrepeat;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserStatusDTO getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatusDTO userStatus) {
        this.userStatus = userStatus;
    }

    public static final class Builder {
        private Integer id;
        private String name;
        private String lastName;
        private String middleName;
        private String additionalInfo;
        private UserRoleDTO role;
        private UserStatusDTO userStatus;
        private String address;
        private String city;
        private String login;
        private String password;
        private String phone;

        private Builder() {
        }

        public Builder name(String val) {
            name = val;
            return this;
        }

        public Builder lastName(String val) {
            lastName = val;
            return this;
        }

        public Builder middleName(String val) {
            middleName = val;
            return this;
        }

        public Builder additionalInfo(String val) {
            additionalInfo = val;
            return this;
        }

        public Builder role(UserRoleDTO val) {
            role = val;
            return this;
        }

        public Builder userStatus(UserStatusDTO val) {
            userStatus = val;
            return this;
        }

        public Builder address(String val) {
            address = val;
            return this;
        }

        public Builder city(String val) {
            city = val;
            return this;
        }

        public Builder login(String val) {
            login = val;
            return this;
        }

        public Builder password(String val) {
            password = val;
            return this;
        }

        public Builder phone(String val) {
            phone = val;
            return this;
        }

        public UserDTO build() {
            return new UserDTO(this);
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}