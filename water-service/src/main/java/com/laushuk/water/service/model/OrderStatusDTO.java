package com.laushuk.water.service.model;

public enum OrderStatusDTO {
    NEW, REVIEWING, IN_PROGRESS, DELIVERED;
}
