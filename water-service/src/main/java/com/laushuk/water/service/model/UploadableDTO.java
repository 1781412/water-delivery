package com.laushuk.water.service.model;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface UploadableDTO {

    MultipartFile getFile();
}
