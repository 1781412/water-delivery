package com.laushuk.water.service.impl;

import com.laushuk.water.repository.UserDao;
import com.laushuk.water.repository.model.User;
import com.laushuk.water.service.model.AppUserPrincipal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Service(value = "appUserDetailsService")
public class AppUserDetailsServiceImpl implements UserDetailsService{

    private final UserDao userDao;

    @Autowired
    public AppUserDetailsServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) {
        User user = userDao.getByLogin(username);
        if(user == null){
            throw new UsernameNotFoundException(username);
        }
        return new AppUserPrincipal(user);
    }
}
