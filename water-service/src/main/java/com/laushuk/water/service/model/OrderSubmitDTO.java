package com.laushuk.water.service.model;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class OrderSubmitDTO {
    private UserDTOFull userDTOFull;
    private String addressInput;
    private Integer selectedAddressId;
    private String city;
    private String address;
    private String phoneInput;
    private Integer selectPhone;
    private String phone;
    private String time;
    private String date;
    private String info;
    private Integer oldOrderId;

    public UserDTOFull getUserDTOFull() {
        return userDTOFull;
    }

    public void setUserDTOFull(UserDTOFull userDTOFull) {
        this.userDTOFull = userDTOFull;
    }

    public String getAddressInput() {
        return addressInput;
    }

    public void setAddressInput(String addressInput) {
        this.addressInput = addressInput;
    }

    public Integer getSelectedAddressId() {
        return selectedAddressId;
    }

    public void setSelectedAddressId(Integer selectedAddressId) {
        this.selectedAddressId = selectedAddressId;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneInput() {
        return phoneInput;
    }

    public void setPhoneInput(String phoneInput) {
        this.phoneInput = phoneInput;
    }

    public Integer getSelectPhone() {
        return selectPhone;
    }

    public void setSelectPhone(Integer selectPhone) {
        this.selectPhone = selectPhone;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public Integer getOldOrderId() {
        return oldOrderId;
    }

    public void setOldOrderId(Integer oldOrderId) {
        this.oldOrderId = oldOrderId;
    }
}
