package com.laushuk.water.service.converter;

import com.laushuk.water.repository.model.Message;
import com.laushuk.water.repository.model.News;
import com.laushuk.water.service.model.MessageDTO;
import com.laushuk.water.service.model.PostDTO;
import org.apache.log4j.Logger;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class MessageConverter {

    private MessageConverter() {
    }

    public static Message setMessage(MessageDTO messageDTO) {
        Message message = new Message();
        message.setId(messageDTO.getId());
        message.setEmail(messageDTO.getEmail());
        message.setSubject(messageDTO.getSubject());
        message.setMessage(messageDTO.getMessage());
        message.setDate(messageDTO.getDate());
        if(message.getUser() != null) {
            message.setUser(UserConverter.setUser(messageDTO.getUserDTO()));
        }
        return message;
    }

    public static MessageDTO setMessageDTO(Message message) {
        MessageDTO messageDTO = new MessageDTO();
        messageDTO.setId(message.getId());
        messageDTO.setEmail(message.getEmail());
        messageDTO.setSubject(message.getSubject());
        messageDTO.setMessage(message.getMessage());
        messageDTO.setDate(message.getDate());
        if(messageDTO.getUserDTO() != null) {
            messageDTO.setUserDTO(UserConverter.setUserDTO(message.getUser()));
        }
        return messageDTO;

    }
}
