package com.laushuk.water.service.converter;

import com.laushuk.water.repository.model.Product;
import com.laushuk.water.service.model.ProductDTO;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class ProductConverter {

    private ProductConverter() {
    }

    public static Product setProduct(Product product, ProductDTO productDTO) {
        product.setId(productDTO.getId());
        product.setInvNum(productDTO.getInvNum());
        product.setName(productDTO.getName());
        product.setDescription(productDTO.getDescription());
        product.setPrice(productDTO.getPrice());
        product.setAvailable(productDTO.getAvailable());
        /*product.setImagePath(productDTO.getImagePath());*/
        product.setCreationDate(productDTO.getCreationDate());

        return product;
    }

    public static ProductDTO setProductDTO(Product product) {
        ProductDTO productDTO = new ProductDTO();
        productDTO.setId(product.getId());
        productDTO.setInvNum(product.getInvNum());
        productDTO.setName(product.getName());
        productDTO.setDescription(product.getDescription());
        productDTO.setPrice(product.getPrice());
        productDTO.setAvailable(product.getAvailable());
        productDTO.setUserDTO(UserConverter.setUserDTO(product.getUser()));
        productDTO.setCreationDate(product.getCreationDate());
        if (product.getFileEntity() != null) {
            productDTO.setImageId(product.getFileEntity().getPostId());
        }

        return productDTO;
    }

}
