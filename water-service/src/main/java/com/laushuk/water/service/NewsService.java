package com.laushuk.water.service;

import com.laushuk.water.service.model.PostDTO;

import java.io.IOException;
import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface NewsService {
    void save(PostDTO postDTO) throws IOException;
    List<PostDTO> getPaginatedPosts(int page, int visible, int postPerPage);
    Long getPagesQuantity(int visible, int postPerPage);
    PostDTO getPostById(int id);
    int deleteByIds(int[] ids);
    int publish(int id);
}
