package com.laushuk.water.service.impl;


import com.laushuk.water.repository.BasketDao;
import com.laushuk.water.repository.OrderDao;
import com.laushuk.water.repository.ProductDao;
import com.laushuk.water.repository.UserDao;
import com.laushuk.water.repository.model.*;
import com.laushuk.water.service.OrderService;
import com.laushuk.water.service.UserService;
import com.laushuk.water.service.converter.OrderConverter;
import com.laushuk.water.service.model.OrderDTO;
import com.laushuk.water.service.model.OrderSubmitDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Service
public class OrderServiceImpl implements OrderService {
    private static Logger log = Logger.getLogger(OrderService.class);
    private OrderDao orderDao;
    private UserDao userDao;
    private BasketDao basketDao;
    private ProductDao productDao;

    @Autowired
    public OrderServiceImpl(OrderDao orderDao, UserDao userDao, BasketDao basketDao, ProductDao productDao) {
        this.orderDao = orderDao;
        this.userDao = userDao;
        this.basketDao = basketDao;
        this.productDao = productDao;
    }

    @Override
    @Transactional
    public int saveOrUpdate(OrderSubmitDTO orderSubmitDTO) {

        User user = userDao.findById(UserService.getUserId());
        UserAddress userAddress = null;
        if (orderSubmitDTO.getAddressInput().equals("choose")) {
            Set<UserAddress> addresses = user.getAddresses();
            for (UserAddress address : addresses) {
                if (address.getId().equals(orderSubmitDTO.getSelectedAddressId())) {
                    userAddress = address;
                    break;
                }
            }
        } else {
            userAddress = new UserAddress();
            userAddress.setUserId(user.getId());
            userAddress.setCity(orderSubmitDTO.getCity());
            userAddress.setAddress(orderSubmitDTO.getAddress());
        }

        UserPhone userPhone = null;
        if (orderSubmitDTO.getPhoneInput().equals("choose")) {
            Set<UserPhone> phones = user.getPhones();
            for (UserPhone phone : phones) {
                if (phone.getId().equals(orderSubmitDTO.getSelectPhone())) {
                    userPhone = phone;
                    break;
                }
            }
        } else {
            userPhone = new UserPhone();
            userPhone.setUserId(user.getId());
            userPhone.setPhone(orderSubmitDTO.getPhone());
        }

        OrderDeliveryTime time = null;
        switch (orderSubmitDTO.getTime()) {
            case "9:00 - 13:00":
                time = OrderDeliveryTime.MORNING;
                break;
            case "13:00 - 17:00":
                time = OrderDeliveryTime.MIDDAY;
                break;
            case "17:00 - 21:00":
                time = OrderDeliveryTime.EVENING;
                break;
            default:
                time = OrderDeliveryTime.MORNING;
        }

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date deliveryDate = null;
        try {
            deliveryDate = dateFormat.parse(orderSubmitDTO.getDate());
        } catch (ParseException e) {
            log.error("Date parsing error: " + e);
        }

        Order order = new Order();
        order.setStatus(OrderStatus.NEW);
        /*order.setCreationDate(new Date());*/
        order.setDeliveryDate(deliveryDate);
        order.setDeliveryTime(time);
        order.setDeliveryAddress(userAddress);
        order.setDeliveryPhone(userPhone);
        order.setDeliveryInformation(orderSubmitDTO.getInfo());

        if (orderSubmitDTO.getOldOrderId() != null) {
            order.setId(orderSubmitDTO.getOldOrderId());
        }

        order.setUser(user);

        BigDecimal total = new BigDecimal(0);
        List<Basket> basket = basketDao.getByUserId(user.getId(),null);
        if (basket != null && !basket.isEmpty()) {
            Product product;

            for (Basket item : basket) {
                product = productDao.findById(item.getProduct().getId());
                total = total.add(product.getPrice().multiply(BigDecimal.valueOf(item.getQuantity())));
                order.getBasket().add(item);
            }
        }
        log.debug("Basket is added to order: " + order.getBasket());
        order.setCost(total);
        orderDao.saveOrUpdate(order);
        basketDao.clearBasket(UserService.getUserId());
        return 1;
    }

    @Override
    @Transactional
    public OrderDTO getById(Integer id) {
        Order order = orderDao.findById(id);
        return OrderConverter.setOrderDTO(order);
    }

    @Override
    @Transactional
    public int updateStatus(Integer id, String status) {
        OrderStatus orderStatus = OrderStatus.valueOf(status);
        Order order = orderDao.findById(id);
        order.setStatus(orderStatus);
        orderDao.saveOrUpdate(order);
        return 1;
    }

    @Override
    @Transactional
    public List<OrderDTO> getPaginated(int page, String city, int perPage) {
        log.debug("Loading orders page " + page + " started");
        List<Order> orders = null;
        List<OrderDTO> orderDTOList = new LinkedList<>();
        orders = orderDao.getPaginatedItems(page, city, perPage);
        for (Order order : orders) {
            orderDTOList.add(OrderConverter.setOrderDTO(order));
        }
        return orderDTOList;
    }

    @Override
    @Transactional
    public List<OrderDTO> getPaginated(int page, int perPage) {
        log.debug("Loading orders page " + page + " started");
        List<OrderDTO> orderDTOList = new LinkedList<>();
        User user = userDao.findById(UserService.getUserId());
        List<Order> orders = orderDao.getPaginatedItems(page, user, perPage);
        for (Order order : orders) {
            orderDTOList.add(OrderConverter.setOrderDTO(order));
        }
        return orderDTOList;
    }

    @Override
    @Transactional
    public Long getPagesQuantity(String city, int perPage) {
        Long itemsQuantity = orderDao.getPostsQuantity(city);
        return itemsQuantity % perPage == 0 ? itemsQuantity / perPage : (itemsQuantity / perPage) + 1;
    }

    @Override
    @Transactional
    public Long getPagesQuantity(int perPage) {
        User user = userDao.findById(UserService.getUserId());
        Long itemsQuantity = orderDao.getPostsQuantity(user);
        return itemsQuantity % perPage == 0 ? itemsQuantity / perPage : (itemsQuantity / perPage) + 1;
    }

    @Override
    @Transactional
    public OrderDTO putIntoBasketById(int orderId) {
        Order order = orderDao.findById(orderId);
        OrderDTO orderDTO = OrderConverter.setOrderDTO(order);
        Set<Basket> basket = order.getBasket();
        for (Basket basketItem : basket) {
            Basket basketNewItem = new Basket();
            basketNewItem.setProduct(basketItem.getProduct());
            basketNewItem.setQuantity(basketItem.getQuantity());
            basketNewItem.setUserId(basketItem.getUserId());
            basketDao.save(basketNewItem);
        }
        return orderDTO;
    }

    @Override
    @Transactional
    public int removeById(Integer orderId) {
        orderDao.deleteById(orderId);
        return 0;
    }
}