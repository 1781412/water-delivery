package com.laushuk.water.service.impl;

import com.laushuk.water.repository.NewsDao;
import com.laushuk.water.repository.UserDao;
import com.laushuk.water.repository.model.FileEntity;
import com.laushuk.water.repository.model.News;
import com.laushuk.water.repository.model.User;
import com.laushuk.water.service.FileService;
import com.laushuk.water.service.NewsService;
import com.laushuk.water.service.UserService;
import com.laushuk.water.service.converter.NewsConverter;
import com.laushuk.water.service.model.PostDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Service
public class NewsServiceImpl implements NewsService {
    private static Logger log = Logger.getLogger(NewsServiceImpl.class);
    private NewsDao newsDao;
    private UserDao userDao;
    private FileService fileService;

    @Autowired
    public NewsServiceImpl(NewsDao newsDao, UserDao userDao, FileService fileService) {
        this.newsDao = newsDao;
        this.userDao = userDao;
        this.fileService = fileService;
    }

    /*нужно сделать, чтобы не загружал фотку в случае ошибки сохранения новости,
     * т.е. новость должна грузиться первой*/
    @Override
    @Transactional
    public void save(PostDTO postDTO) throws IOException {
        log.debug("Saving new publication");
        News post;
        try {
            if (postDTO.getId() == null) {
                post = new News();
            } else {
                post = newsDao.findById(postDTO.getId());
            }
            post = NewsConverter.setPost(post, postDTO);
            FileEntity fileEntity = fileService.getFileEntity(postDTO);
            if (!fileEntity.getFileName().equals("")) {
                post.setPostFileEntity(fileEntity);
            }
            User user = userDao.findById(UserService.getUserId());
            post.setUser(user);
            newsDao.saveOrUpdate(post);
        } catch (IOException e) {
            log.error("Error during post saving" + e);
            throw e;
        }
    }

    @Override
    @Transactional
    public PostDTO getPostById(int id) {
        log.debug("Loading post id: " + id + " started");
        News post = newsDao.findById(id);
        PostDTO postDTO = NewsConverter.setPostDTO(post);
        return postDTO;
    }


    @Override
    @Transactional
    public List<PostDTO> getPaginatedPosts(int page, int visible, int postPerPage) {
        log.debug("Loading news page " + page + " started");
        List<PostDTO> newsDTOList = new LinkedList<>();
        List<News> news = newsDao.getPaginatedPosts(page, visible, postPerPage);
        for (News post : news) {
            newsDTOList.add(NewsConverter.setPostDTO(post));
        }
        return newsDTOList;
    }

    @Override
    @Transactional
    public Long getPagesQuantity(int visible, int postPerPage) {
        Long postsQuantity = newsDao.getPostsQuantity(visible);
        return postsQuantity % postPerPage == 0 ? postsQuantity / postPerPage : (postsQuantity / postPerPage) + 1;
    }

    @Override
    @Transactional
    public int deleteByIds(int[] ids) {
        int result = 0;
        for (int id : ids) {
            newsDao.delete(newsDao.findById(id));
            result++;
        }
        return result;
    }

    @Override
    @Transactional
    public int publish(int id) {
        newsDao.publish(id);
        return 0;
    }
}
