package com.laushuk.water.service.converter;

import com.laushuk.water.repository.model.UserAddress;
import com.laushuk.water.service.model.UserAddressDTO;
import org.apache.log4j.Logger;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class UserAddressConverter {
    private static final Logger logger = Logger.getLogger(UserAddressConverter.class);

    private UserAddressConverter() {
    }

    public static UserAddress setUserAddress(UserAddressDTO userAddressDTO) {
        UserAddress userAddress = new UserAddress();
        userAddress.setId(userAddressDTO.getId());
        userAddress.setUserId(userAddressDTO.getUserId());
        userAddress.setCity(userAddressDTO.getCity());
        userAddress.setAddress(userAddressDTO.getAddress());
        return userAddress;
    }

    public static UserAddressDTO setUserAddressDTO(UserAddress userAddress) {
        UserAddressDTO userAddressDTO = new UserAddressDTO();
        userAddressDTO.setId(userAddress.getId());
        userAddressDTO.setUserId(userAddress.getUserId());
        userAddressDTO.setCity(userAddress.getCity());
        userAddressDTO.setAddress(userAddress.getAddress());
        return userAddressDTO;
    }
}
