package com.laushuk.water.service.impl;

import com.laushuk.water.repository.UserDao;
import com.laushuk.water.repository.model.User;
import com.laushuk.water.repository.model.UserRole;
import com.laushuk.water.repository.model.UserStatus;
import com.laushuk.water.service.UserService;
import com.laushuk.water.service.converter.UserConverter;
import com.laushuk.water.service.model.UserDTO;
import com.laushuk.water.service.model.UserDTOFull;
import com.laushuk.water.service.model.UserRoleDTO;
import com.laushuk.water.service.model.UserStatusDTO;
import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Service
public class UserServiceImpl implements UserService {

    private static final Logger log = Logger.getLogger(UserServiceImpl.class);
    private final UserDao userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    @Transactional
    public UserDTOFull get(Integer id) {
        if (id != null) {
            User user = userDao.findById(id);
            log.debug(user);
            UserDTOFull userDTO = UserConverter.set(user);
            log.debug(userDTO);
            return userDTO;
        }
        return null;
    }

    @Override
    @Transactional
    public UserDTOFull get() {
        return get(UserService.getUserId());
    }


    @Override
    @Transactional
    public boolean isExist(String login) {
        return userDao.isExist(login);
    }


    @Override
    @Transactional
    public int save(UserDTO userDTO) {
        log.debug("login " + userDTO.getLogin() + " is not exist, saving new User");
        User user = UserConverter.setUser(userDTO);
        user.setPassword(bCryptPasswordEncoder.encode(userDTO.getPassword()));
        userDao.saveOrUpdate(user);
        return 0;
    }

    @Override
    @Transactional
    public int updateRole(Integer id, String role) {
        UserRole userRole = UserRole.valueOf(role);
        User user = userDao.findById(id);
        user.setRole(userRole);
        userDao.saveOrUpdate(user);
        return 1;
    }

    @Override
    @Transactional
    public int updateStatus(Integer id, String status) {
        UserStatus userStatus = UserStatus.valueOf(status);
        User user = userDao.findById(id);
        user.setUserStatus(userStatus);
        userDao.saveOrUpdate(user);
        return 1;
    }

    @Override
    @Transactional
    public int updatePassword(Integer id, String password) {
        User user = userDao.findById(id);
        user.setPassword(bCryptPasswordEncoder.encode(password));
        userDao.saveOrUpdate(user);
        return 1;
    }

    @Override
    @Transactional
    public List<UserDTO> getPaginatedUsers(int page, UserStatusDTO statusDTO, UserRoleDTO roleDTO, int usersPerPage) {
        log.debug("Loading news page " + page + " started");

        List<UserDTO> userDTOList = new LinkedList<>();
        UserStatus status = null;
        if (statusDTO != null) {
            status = UserStatus.valueOf(statusDTO.toString());
        }
        UserRole role = null;
        if (roleDTO != null) {
            role = UserRole.valueOf(roleDTO.toString());
        }

        List<User> users = userDao.getPaginatedUsers(page, status, role, usersPerPage);
        for (User user : users) {
            userDTOList.add(UserConverter.setUserDTO(user));
        }
        return userDTOList;
    }

    @Override
    @Transactional
    public Long getPagesQuantity(UserStatusDTO statusDTO, UserRoleDTO roleDTO, Integer itemPerPage) {

        UserStatus status = null;
        if (statusDTO != null) {
            status = UserStatus.valueOf(statusDTO.toString());
        }
        UserRole role = null;
        if (roleDTO != null) {
            role = UserRole.valueOf(roleDTO.toString());
        }

        Long itemQuantity = userDao.getItemQuantity(status, role);

        return itemQuantity % itemPerPage == 0 ? itemQuantity / itemPerPage : (itemQuantity / itemPerPage) + 1;
    }

    @Override
    @Transactional
    public int deleteByIds(String[] ids) {
        int result = 0;
        for (String id : ids) {
            userDao.delete(userDao.findById(Integer.valueOf(id)));
            result++;
        }
        return result;
    }
}