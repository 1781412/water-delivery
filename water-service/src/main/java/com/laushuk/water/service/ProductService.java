package com.laushuk.water.service;

import com.laushuk.water.service.model.ProductDTO;

import java.io.IOException;
import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface ProductService {
    void save(ProductDTO productDTO) throws IOException;
    List<ProductDTO> getPaginatedItems(int page, int available, int itemsPerPage);
    Long getPagesQuantity(Integer available, Integer itemPerPage);
    ProductDTO get(int id);
    int updateStatus(Integer id, Boolean status);
    ProductDTO copy(Integer id);
}
