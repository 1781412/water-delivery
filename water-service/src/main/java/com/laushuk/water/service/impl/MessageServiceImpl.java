package com.laushuk.water.service.impl;

import com.laushuk.water.repository.MessageDao;
import com.laushuk.water.repository.NewsDao;
import com.laushuk.water.repository.UserDao;
import com.laushuk.water.repository.model.FileEntity;
import com.laushuk.water.repository.model.Message;
import com.laushuk.water.repository.model.News;
import com.laushuk.water.repository.model.User;
import com.laushuk.water.service.FileService;
import com.laushuk.water.service.MessageService;
import com.laushuk.water.service.NewsService;
import com.laushuk.water.service.UserService;
import com.laushuk.water.service.converter.MessageConverter;
import com.laushuk.water.service.converter.NewsConverter;
import com.laushuk.water.service.model.MessageDTO;
import com.laushuk.water.service.model.PostDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Service
public class MessageServiceImpl implements MessageService {
    private static Logger log = Logger.getLogger(MessageServiceImpl.class);
    private MessageDao messageDao;

    @Autowired
    public MessageServiceImpl(MessageDao messageDao) {
        this.messageDao = messageDao;
    }

    @Override
    @Transactional
    public void save(MessageDTO messageDTO) {
        log.debug("Saving new message");
        Message message = MessageConverter.setMessage(messageDTO);
        message.setDate(new Date(System.currentTimeMillis()));
        messageDao.save(message);
    }

    @Override
    @Transactional
    public MessageDTO getById(Integer id) {
        log.debug("Loading message id: " + id + " started");
        Message message = messageDao.findById(id);
        MessageDTO messageDTO = MessageConverter.setMessageDTO(message);
        return messageDTO;
    }


    @Override
    @Transactional
    public List<MessageDTO> getPaginated(Integer page, Integer postPerPage) {
        log.debug("Loading messages page " + page + " started");
        List<MessageDTO> messageDTOList = new LinkedList<>();
        List<Message> messages = messageDao.getPaginated(page, postPerPage);
        for (Message message : messages) {
            messageDTOList.add(MessageConverter.setMessageDTO(message));
        }
        return messageDTOList;
    }

    @Override
    @Transactional
    public Long getPagesQuantity(Integer postPerPage) {
        Long postsQuantity = messageDao.getPostsQuantity();
        return postsQuantity % postPerPage == 0 ? postsQuantity / postPerPage : (postsQuantity / postPerPage) + 1;
    }

    @Override
    @Transactional
    public int deleteByIds(int[] ids) {
        int result = 0;
        for (int id : ids) {
            messageDao.delete(messageDao.findById(id));
            result++;
        }
        return result;
    }

}
