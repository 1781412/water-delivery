package com.laushuk.water.service;

import com.laushuk.water.service.model.OrderDTO;
import com.laushuk.water.service.model.OrderSubmitDTO;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface OrderService {

    int saveOrUpdate(OrderSubmitDTO orderSubmitDTO);

    OrderDTO getById(Integer id);

    int updateStatus(Integer id, String status);

    List<OrderDTO> getPaginated(int page, String city, int perPage);

    List<OrderDTO> getPaginated(int page, int perPage);

    Long getPagesQuantity(String city, int perPage);

    Long getPagesQuantity(int perPage);

    OrderDTO putIntoBasketById(int orderId);

    int removeById(Integer orderId);
}
