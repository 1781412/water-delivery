package com.laushuk.water.service.model;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class UserPhoneDTO {
    private Integer id;
    private String phone;
    private Integer userId;

    private UserPhoneDTO(Builder builder) {
        setId(builder.id);
        setPhone(builder.phone);
        setUserId(builder.userId);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }


    public static final class Builder {
        private Integer id;
        private String phone;
        private Integer userId;

        private Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder phone(String val) {
            phone = val;
            return this;
        }

        public Builder userId(Integer val) {
            userId = val;
            return this;
        }

        public UserPhoneDTO build() {
            return new UserPhoneDTO(this);
        }
    }
}
