package com.laushuk.water.service.model;

import java.math.BigDecimal;
import java.util.Set;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class BigBasketDTO {
    private Set<BasketDTO> basketDTOS;
    private BigDecimal totalCost;

    public void set(Set<BasketDTO> basketDTOS, BigDecimal totalCost) {
        this.basketDTOS = basketDTOS;
        this.totalCost = totalCost;
    }

    public Set<BasketDTO> getBasketDTOS() {
        return basketDTOS;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }


}
