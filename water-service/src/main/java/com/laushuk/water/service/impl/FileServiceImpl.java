package com.laushuk.water.service.impl;

import com.laushuk.water.repository.FileDao;
import com.laushuk.water.repository.model.FileEntity;
import com.laushuk.water.service.FileService;
import com.laushuk.water.service.model.UploadableDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Service
public class FileServiceImpl implements FileService{

    private FileDao fileDao;
    private Properties properties;

    @Autowired
    public FileServiceImpl(FileDao fileDao, Properties properties) {
        this.fileDao = fileDao;
        this.properties = properties;
    }

    @Override
    @Transactional
    public File getFileById (Long id) {
        FileEntity fileEntity = fileDao.findById(Math.toIntExact(id));
        return new File (fileEntity.getLocation());
    }

    @Override
    public FileEntity getFileEntity(UploadableDTO dto) throws IOException {
        String fileLocation = properties.getProperty("upload.location") + System.currentTimeMillis() + ".jpg";
        FileCopyUtils.copy(dto.getFile().getBytes(), new File(fileLocation));
        FileEntity fileEntity = new FileEntity();
        fileEntity.setLocation(fileLocation);
        fileEntity.setFileName(dto.getFile().getOriginalFilename());
        return fileEntity;
    }
}
