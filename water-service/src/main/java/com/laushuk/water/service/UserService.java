package com.laushuk.water.service;

import com.laushuk.water.service.model.*;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface UserService {
    List<UserDTO> getPaginatedUsers(int page, UserStatusDTO statusDTO, UserRoleDTO roleDTO, int usersPerPage);
    Long getPagesQuantity(UserStatusDTO statusDTO, UserRoleDTO roleDTO, Integer itemPerPage);
    int deleteByIds(String[] ids);
    int updateRole(Integer id, String role);
    int updateStatus(Integer id, String status);
    UserDTOFull get(Integer id);
    int updatePassword(Integer id, String password);
    int save(UserDTO userDTO);

    UserDTOFull get();

    static Integer getUserId() {
        AppUserPrincipal principal = (AppUserPrincipal) SecurityContextHolder.getContext()
                .getAuthentication()
                .getPrincipal();
        return principal.getUserId();
    }

    boolean isExist(String login);
}
