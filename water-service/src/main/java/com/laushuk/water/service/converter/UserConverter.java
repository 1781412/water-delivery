package com.laushuk.water.service.converter;

import com.laushuk.water.repository.model.*;
import com.laushuk.water.service.model.*;
import org.apache.log4j.Logger;

import java.util.Date;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class UserConverter {
    private static final Logger log = Logger.getLogger(UserConverter.class);

    private UserConverter() {
    }

    public static User setUser(UserDTO userDTO) {
        User user = new User();
        user.setId(userDTO.getId());
        user.setLogin(userDTO.getLogin());
        user.setRole(UserRole.valueOf(userDTO.getRole().toString()));
        user.setUserStatus(UserStatus.valueOf(userDTO.getUserStatus().toString()));
        user.setName(userDTO.getName());

        UserAddress userAddress = new UserAddress();
        userAddress.setCity(userDTO.getCity());
        userAddress.setAddress(userDTO.getAddress());
        user.getAddresses().add(userAddress);

        UserPhone userPhone = new UserPhone();
        userPhone.setPhone(userDTO.getPhone());
        user.getPhones().add(userPhone);

        UserInformation userInformation = new UserInformation();
        userInformation.setLastName(userDTO.getLastName());
        userInformation.setMiddleName(userDTO.getMiddleName());
        userInformation.setAdditionalInformation(userDTO.getAdditionalInfo());
        userInformation.setRegistrationDate(new Date());
        user.setUserInformation(userInformation);
        //for bidirectional one-to-one
        userInformation.setUser(user);

        log.debug(user.toString());
        return user;
    }

    public static UserDTO setUserDTO(User user) {
        UserDTO userDTO = new UserDTO(user);
        try {
            userDTO.setLastName(user.getUserInformation().getLastName());
        } catch (NullPointerException e) {
            log.debug("Last name is unknown" + e);
            userDTO.setLastName("Unknown");
        }
        return userDTO;
    }

    public static UserDTOFull set(User user) {
        UserDTOFull u = new UserDTOFull();

        u.setId(user.getId());

        u.setName(user.getName());

        try {
            u.setLastName(user.getUserInformation().getLastName());
        } catch (NullPointerException e) {
            log.debug("Last name is unknown" + e);
            u.setLastName("Unknown");
        }

        try {
            u.setMiddleName(user.getUserInformation().getMiddleName());
        } catch (NullPointerException e) {
            log.debug("Middle name is unknown" + e);
            u.setMiddleName("Unknown");
        }

        try {
            u.setAdditionalInfo(user.getUserInformation().getAdditionalInformation());
        } catch (NullPointerException e) {
            log.debug("No additional info" + e);
            u.setAdditionalInfo("-");
        }

        u.setRole(UserRoleDTO.valueOf(user.getRole().toString()));

        u.setUserStatus(UserStatusDTO.valueOf(user.getUserStatus().toString()));

        try {
            for (UserAddress userAddress : user.getAddresses()) {
                UserAddressDTO ua = new UserAddressDTO();
                ua.setId(userAddress.getId());
                ua.setCity(userAddress.getCity());
                if (u.getCity() == null) {
                    u.setCity(userAddress.getCity());
                }
                ua.setAddress(userAddress.getAddress());
                ua.setUserId(userAddress.getUserId()); //c ней не сетапится
                u.getAddresses().add(ua);
            }
        } catch (NullPointerException e) {
            log.debug("Error during setting addresses" + e);
        }

        u.setLogin(user.getLogin());

        u.setPassword(user.getPassword());

        try {
            for (UserPhone userPhone : user.getPhones()) {
                u.getPhones().add(UserPhoneConverter.setUserPhoneDTO(userPhone));
            }
        } catch (NullPointerException e) {
            log.debug("Error during setting phones" + e);
        }

        return u;
    }
}
