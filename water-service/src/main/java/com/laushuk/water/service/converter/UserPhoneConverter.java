package com.laushuk.water.service.converter;

import com.laushuk.water.repository.model.UserPhone;
import com.laushuk.water.service.model.UserPhoneDTO;
import org.apache.log4j.Logger;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class UserPhoneConverter {
    private static final Logger logger = Logger.getLogger(UserPhoneConverter.class);

    private UserPhoneConverter() {
    }

    public static UserPhone setUserPhone(UserPhoneDTO userPhoneDTO) {
        UserPhone userPhone = new UserPhone();
        userPhone.setId(userPhoneDTO.getId());
        userPhone.setUserId(userPhoneDTO.getUserId());
        userPhone.setPhone(userPhoneDTO.getPhone());
        return userPhone;
    }

    public static UserPhoneDTO setUserPhoneDTO(UserPhone userPhone) {
        return UserPhoneDTO.newBuilder()
                .id(userPhone.getId())
                .userId(userPhone.getUserId())
                .phone(userPhone.getPhone())
                .build();
    }
}
