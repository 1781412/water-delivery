package com.laushuk.water.service;

import com.laushuk.water.repository.model.FileEntity;
import com.laushuk.water.service.model.UploadableDTO;

import java.io.File;
import java.io.IOException;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface FileService {
    File getFileById (Long id);

    FileEntity getFileEntity(UploadableDTO dto) throws IOException;
}
