package com.laushuk.water.service;

import com.laushuk.water.service.model.MessageDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by p.laushuk on 01.09.2017. Email:1781412@gmail.com
 */
public interface MessageService {
    @Transactional
    void save(MessageDTO messageDTO);

    @Transactional
    MessageDTO getById(Integer id);

    @Transactional
    List<MessageDTO> getPaginated(Integer page, Integer postPerPage);

    @Transactional
    Long getPagesQuantity(Integer postPerPage);

    @Transactional
    int deleteByIds(int[] ids);
}
