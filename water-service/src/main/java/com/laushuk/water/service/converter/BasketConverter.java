package com.laushuk.water.service.converter;

import com.laushuk.water.repository.model.Basket;
import com.laushuk.water.repository.model.Product;
import com.laushuk.water.service.model.BasketDTO;
import org.apache.log4j.Logger;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class BasketConverter {
    private static final Logger logger = Logger.getLogger(BasketConverter.class);

    private BasketConverter() {
    }

    public static Basket setBasket(BasketDTO basketDTO) {
        Basket basket = new Basket();
        basket.setId(basketDTO.getId());
        basket.setUserId(basketDTO.getUserId());
        basket.setOrderId(basketDTO.getOrderId());
        if (basketDTO.getProductDTO() != null) {
            basket.setProduct(ProductConverter.setProduct(new Product(), basketDTO.getProductDTO()));
        }
        basket.setQuantity(basketDTO.getQuantity());
        return basket;
    }

    public static BasketDTO setBasketDTO(Basket basket) {
        BasketDTO basketDTO = new BasketDTO();
        basketDTO.setId(basket.getId());
        basketDTO.setUserId(basket.getUserId());
        basketDTO.setOrderId(basket.getOrderId());
        basketDTO.setProductId(basket.getProduct().getId());
        basketDTO.setProductDTO(ProductConverter.setProductDTO(basket.getProduct()));
        basketDTO.setQuantity(basket.getQuantity());
        return basketDTO;
    }
}
