package com.laushuk.water.service.impl;

import com.laushuk.water.repository.BasketDao;
import com.laushuk.water.repository.ProductDao;
import com.laushuk.water.repository.model.Basket;
import com.laushuk.water.repository.model.Product;
import com.laushuk.water.service.BasketService;
import com.laushuk.water.service.UserService;
import com.laushuk.water.service.converter.BasketConverter;
import com.laushuk.water.service.converter.ProductConverter;
import com.laushuk.water.service.model.BasketDTO;
import com.laushuk.water.service.model.BigBasketDTO;
import com.laushuk.water.service.model.ProductDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Service
public class BasketServiceImpl implements BasketService {
    private static Logger log = Logger.getLogger(BasketServiceImpl.class);
    private BasketDao basketDao;
    private ProductDao productDao;

    @Autowired
    public BasketServiceImpl(BasketDao basketDao, ProductDao productDao) {
        this.basketDao = basketDao;
        this.productDao = productDao;
    }

    @Override
    @Transactional
    public int add(BasketDTO basketDtoItem) {
        log.debug("Adding to basket");
        Basket newItem = null;
        int userId = UserService.getUserId();

        List<Basket> basket = basketDao.getByUserId(userId, null);
        if (!basket.isEmpty()) {
            for (Basket item : basket) {
                if (item.getProduct().getId().equals(basketDtoItem.getProductId())) {
                    item.setQuantity(item.getQuantity() + basketDtoItem.getQuantity());
                    newItem = item;
                    break;
                }
            }
        }
        if (newItem == null) {
            basketDtoItem.setUserId(userId);
            newItem = BasketConverter.setBasket(basketDtoItem);
            if (newItem.getProduct() == null) {
                newItem.setProduct(productDao.findById(basketDtoItem.getProductId()));
            }
            basketDao.saveOrUpdate(newItem);
        }

        return 1;
    }

    @Override
    @Transactional
    public BigBasketDTO getBigBasket() {
        Set<BasketDTO> basketDTO = new HashSet<>();
        BigBasketDTO bigBasket = new BigBasketDTO();
        BigDecimal totalCost = BigDecimal.valueOf(0);

        List<Basket> basket = basketDao.getByUserId(UserService.getUserId(), null);
        if (!basket.isEmpty()) {

            for (Basket item : basket) {
                Product product = productDao.findById(item.getProduct().getId());
                log.debug(item.getProduct().getId());
                log.debug(product);
                ProductDTO productDTO = ProductConverter.setProductDTO(product);
                BasketDTO dtoItem = new BasketDTO();
                dtoItem.setProductDTO(productDTO);
                dtoItem.setQuantity(item.getQuantity());
                dtoItem.setCost(product.getPrice().multiply(BigDecimal.valueOf(item.getQuantity())));
                totalCost = totalCost.add(dtoItem.getCost());
                basketDTO.add(dtoItem);
            }
        }
        bigBasket.set(basketDTO, totalCost);

        return bigBasket;
    }

    @Override
    @Transactional
    public int removeItemByProductId(Integer itemId) {
        return basketDao.removeProductById(UserService.getUserId(), itemId);
    }

    @Override
    @Transactional
    public int clearBasket() {
        return basketDao.clearBasket(UserService.getUserId());
    }
}
