package com.laushuk.water.service.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class OrderDTO {

    private Integer id;                     //required
    private UserDTO userDTO;
    private OrderStatusDTO statusDTO;       //required
    private Date creationDate;
    private Date deliveryDate;
    private OrderDeliveryTimeDTO deliveryTimeDTO;
    private UserAddressDTO deliveryAddressDTO;
    private UserPhoneDTO deliveryPhoneDTO;
    private String deliveryInformation;
    private Set<BasketDTO> basketDTO = new HashSet<>();
    private BigDecimal cost;                //required

    private OrderDTO(Builder builder) {
        setId(builder.id);
        setUserDTO(builder.userDTO);
        setStatusDTO(builder.statusDTO);
        setCreationDate(builder.creationDate);
        setDeliveryDate(builder.deliveryDate);
        setDeliveryTimeDTO(builder.deliveryTimeDTO);
        setDeliveryAddressDTO(builder.deliveryAddressDTO);
        setDeliveryPhoneDTO(builder.deliveryPhoneDTO);
        setDeliveryInformation(builder.deliveryInformation);
        setBasketDTO(builder.basketDTO);
        setCost(builder.cost);
    }

    public static Builder newBuilder() {
        return new Builder();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public UserDTO getUserDTO() {
        return userDTO;
    }

    public void setUserDTO(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public OrderStatusDTO getStatusDTO() {
        return statusDTO;
    }

    public void setStatusDTO(OrderStatusDTO statusDTO) {
        this.statusDTO = statusDTO;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public OrderDeliveryTimeDTO getDeliveryTimeDTO() {
        return deliveryTimeDTO;
    }

    public void setDeliveryTimeDTO(OrderDeliveryTimeDTO deliveryTimeDTO) {
        this.deliveryTimeDTO = deliveryTimeDTO;
    }

    public UserAddressDTO getDeliveryAddressDTO() {
        return deliveryAddressDTO;
    }

    public void setDeliveryAddressDTO(UserAddressDTO deliveryAddressDTO) {
        this.deliveryAddressDTO = deliveryAddressDTO;
    }

    public UserPhoneDTO getDeliveryPhoneDTO() {
        return deliveryPhoneDTO;
    }

    public void setDeliveryPhoneDTO(UserPhoneDTO deliveryPhoneDTO) {
        this.deliveryPhoneDTO = deliveryPhoneDTO;
    }

    public String getDeliveryInformation() {
        return deliveryInformation;
    }

    public void setDeliveryInformation(String deliveryInformation) {
        this.deliveryInformation = deliveryInformation;
    }

    public Set<BasketDTO> getBasketDTO() {
        return basketDTO;
    }

    public void setBasketDTO(Set<BasketDTO> basketDTO) {
        this.basketDTO = basketDTO;
    }

    public BigDecimal getCost() {
        return cost;
    }

    public void setCost(BigDecimal cost) {
        this.cost = cost;
    }

    public static final class Builder {
        private Integer id;
        private UserDTO userDTO;
        private OrderStatusDTO statusDTO;
        private Date creationDate;
        private Date deliveryDate;
        private OrderDeliveryTimeDTO deliveryTimeDTO;
        private UserAddressDTO deliveryAddressDTO;
        private UserPhoneDTO deliveryPhoneDTO;
        private String deliveryInformation;
        private Set<BasketDTO> basketDTO;
        private BigDecimal cost;

        private Builder() {
        }

        public Builder id(Integer val) {
            id = val;
            return this;
        }

        public Builder userDTO(UserDTO val) {
            userDTO = val;
            return this;
        }

        public Builder statusDTO(OrderStatusDTO val) {
            statusDTO = val;
            return this;
        }

        public Builder creationDate(Date val) {
            creationDate = val;
            return this;
        }

        public Builder deliveryDate(Date val) {
            deliveryDate = val;
            return this;
        }

        public Builder deliveryTimeDTO(OrderDeliveryTimeDTO val) {
            deliveryTimeDTO = val;
            return this;
        }

        public Builder deliveryAddressDTO(UserAddressDTO val) {
            deliveryAddressDTO = val;
            return this;
        }

        public Builder deliveryPhoneDTO(UserPhoneDTO val) {
            deliveryPhoneDTO = val;
            return this;
        }

        public Builder deliveryInformation(String val) {
            deliveryInformation = val;
            return this;
        }

        public Builder basketDTO(Set<BasketDTO> val) {
            basketDTO = val;
            return this;
        }

        public Builder cost(BigDecimal val) {
            cost = val;
            return this;
        }

        public OrderDTO build() {
            return new OrderDTO(this);
        }
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "id=" + id +
                ", statusDTO=" + statusDTO +
                ", creationDate=" + creationDate +
                ", deliveryDate=" + deliveryDate +
                ", deliveryTimeDTO=" + deliveryTimeDTO +
                ", deliveryInformation='" + deliveryInformation + '\'' +
                ", basketDTO=" + basketDTO +
                ", cost=" + cost +
                '}';
    }
}
