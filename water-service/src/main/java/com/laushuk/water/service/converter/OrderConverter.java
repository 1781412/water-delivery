package com.laushuk.water.service.converter;

import com.laushuk.water.repository.model.Basket;
import com.laushuk.water.repository.model.Order;
import com.laushuk.water.repository.model.OrderDeliveryTime;
import com.laushuk.water.repository.model.OrderStatus;
import com.laushuk.water.service.model.BasketDTO;
import com.laushuk.water.service.model.OrderDTO;
import com.laushuk.water.service.model.OrderDeliveryTimeDTO;
import com.laushuk.water.service.model.OrderStatusDTO;
import org.apache.log4j.Logger;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class OrderConverter {
    private static final Logger log = Logger.getLogger(OrderConverter.class);

    private OrderConverter() {
    }

    public static Order setOrder(OrderDTO orderDTO) {
        Order order = new Order();
        order.setId(orderDTO.getId());
        if (orderDTO.getUserDTO() != null) {
            order.setUser(UserConverter.setUser(orderDTO.getUserDTO()));
        }
        order.setStatus(OrderStatus.valueOf(orderDTO.getStatusDTO().toString()));
        order.setCreationDate(orderDTO.getCreationDate());
        order.setDeliveryDate(orderDTO.getDeliveryDate());
        order.setDeliveryTime(OrderDeliveryTime.valueOf(orderDTO.getDeliveryTimeDTO().toString()));
        //if (orderDTO.getDeliveryAddressDTO() != null)
        order.setDeliveryAddress(UserAddressConverter.setUserAddress(orderDTO.getDeliveryAddressDTO()));
        //if (orderDTO.getDeliveryPhoneDTO() != null)
        order.setDeliveryPhone(UserPhoneConverter.setUserPhone(orderDTO.getDeliveryPhoneDTO()));
        order.setDeliveryInformation(orderDTO.getDeliveryInformation());
        if (orderDTO.getBasketDTO() != null && !orderDTO.getBasketDTO().isEmpty()) {
            Set<BasketDTO> basketDTOSet = orderDTO.getBasketDTO();
            for (BasketDTO basketDTO : basketDTOSet) {
                order.getBasket().add(BasketConverter.setBasket(basketDTO));
            }
        }
        order.setCost(orderDTO.getCost());
        return order;
    }

    public static OrderDTO setOrderDTO(Order order) {
        OrderDTO orderDTO = OrderDTO.newBuilder()
                .id(order.getId())
                .userDTO(UserConverter.setUserDTO(order.getUser()))
                .statusDTO(OrderStatusDTO.valueOf(order.getStatus().toString()))
                .creationDate(order.getCreationDate())
                .deliveryDate(order.getDeliveryDate())
                .deliveryTimeDTO(OrderDeliveryTimeDTO.valueOf(order.getDeliveryTime().toString()))
                .deliveryAddressDTO(UserAddressConverter.setUserAddressDTO(order.getDeliveryAddress()))
                .deliveryPhoneDTO(UserPhoneConverter.setUserPhoneDTO(order.getDeliveryPhone()))
                .deliveryInformation(order.getDeliveryInformation())
                .cost(order.getCost())
                .basketDTO(new HashSet<>()) // без него null почему-то
                .build();
        log.debug("orderDTO: " + orderDTO);
        if (order.getBasket() != null && !order.getBasket().isEmpty()) {
            Set<Basket> basketSet = order.getBasket();
            for (Basket basket : basketSet) {
                log.debug("basketItem: " + basket);
                orderDTO.getBasketDTO().add(BasketConverter.setBasketDTO(basket));
            }
        }
        return orderDTO;
    }
}
