package com.laushuk.water.service.impl;

import com.laushuk.water.repository.FileDao;
import com.laushuk.water.repository.ProductDao;
import com.laushuk.water.repository.UserDao;
import com.laushuk.water.repository.model.FileEntity;
import com.laushuk.water.repository.model.Product;
import com.laushuk.water.repository.model.User;
import com.laushuk.water.service.FileService;
import com.laushuk.water.service.ProductService;
import com.laushuk.water.service.UserService;
import com.laushuk.water.service.converter.ProductConverter;
import com.laushuk.water.service.model.ProductDTO;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
@Service
public class ProductServiceImpl implements ProductService {
    private static Logger log = Logger.getLogger(ProductServiceImpl.class);
    private ProductDao productDao;
    private UserDao userDao;
    private FileDao fileDao;
    private FileService fileService;

    @Autowired
    public ProductServiceImpl(ProductDao productDao, UserDao userDao, FileDao fileDao, FileService fileService) {
        this.productDao = productDao;
        this.userDao = userDao;
        this.fileDao = fileDao;
        this.fileService = fileService;
    }

    @Override
    @Transactional
    public void save(ProductDTO productDTO) throws IOException {
        log.debug("Saving new product");
        Product product;
        if (productDTO.getId() == null) {
            product = new Product();
        } else {
            product = productDao.findById(productDTO.getId());
        }
        product = ProductConverter.setProduct(product, productDTO);
        FileEntity fileEntity = fileService.getFileEntity(productDTO);
        if (!fileEntity.getFileName().equals("")) {
            product.setFileEntity(fileEntity);
        }
        if (product.getFileEntity()==null) {
            product.setFileEntity(fileDao.findById(productDTO.getImageId()));
        }
        User user = userDao.findById(UserService.getUserId());
        product.setUser(user);
        productDao.saveOrUpdate(product);
    }

    @Override
    @Transactional
    public ProductDTO get(int id) {
        log.debug("Loading product id: " + id + " started");
        ProductDTO productDTO;
        Product product = productDao.findById(id);
        Long used = productDao.isUsed(id);
        productDTO = ProductConverter.setProductDTO(product);
        productDTO.setIsUsed(used > 0);
        return productDTO;
    }

    @Override
    @Transactional
    public List<ProductDTO> getPaginatedItems(int page, int available, int itemsPerPage) {
        log.debug("Loading products page " + page + " started");
        List<ProductDTO> productDTOList = new LinkedList<>();
        List<Product> products = productDao.getPaginatedItems(page, available, itemsPerPage);
        for (Product item : products) {
            productDTOList.add(ProductConverter.setProductDTO(item));
        }
        return productDTOList;
    }

    @Override
    @Transactional
    public Long getPagesQuantity(Integer available, Integer itemPerPage) {
        Long quantity = productDao.getRowsQuantity(available);
        return quantity % itemPerPage == 0 ? quantity / itemPerPage : (quantity / itemPerPage) + 1;
    }

    @Override
    @Transactional
    public int updateStatus(Integer id, Boolean status) {
        Product product = productDao.findById(id);
        product.setAvailable(status);
        productDao.saveOrUpdate(product);
        return 1;
    }

    @Override
    @Transactional
    public ProductDTO copy(Integer id) {
        Product product = productDao.findById(id);
        ProductDTO productDTO = ProductConverter.setProductDTO(product);
        productDTO.setId(null);
        productDTO.setInvNum(productDTO.getInvNum() + " copy");
        productDTO.setCreationDate(new Date(System.currentTimeMillis()));
        return productDTO;
    }
}
