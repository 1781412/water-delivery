package com.laushuk.water.service.model;

public enum UserRoleDTO {
    ROLE_SUPERADMIN, ROLE_ADMIN, ROLE_USER,
    INVALID
}
