package com.laushuk.water.service;

import com.laushuk.water.service.model.BasketDTO;
import com.laushuk.water.service.model.BigBasketDTO;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public interface BasketService {
    int add(BasketDTO basketDtoItem);

    BigBasketDTO getBigBasket();

    int removeItemByProductId(Integer itemId);

    int clearBasket();
}
