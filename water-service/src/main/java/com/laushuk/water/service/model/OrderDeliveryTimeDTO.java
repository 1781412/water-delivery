package com.laushuk.water.service.model;

public enum OrderDeliveryTimeDTO {
    MORNING, MIDDAY, EVENING;
}
