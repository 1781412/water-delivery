package com.laushuk.water.service.model;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Piotr Laushuk on 27.08.2017. E-mail: 1781412@gmail.com
 */
public class UserDTOFull {
    private Integer id;
    private String name;
    private String lastName;
    private String middleName;
    private String additionalInfo;
    private UserRoleDTO role;
    private UserStatusDTO userStatus;
    private Set<UserAddressDTO> addresses = new HashSet<>();
    private String city;
    private String login;
    private String password;
    private Set<UserPhoneDTO> phones = new HashSet<>();

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public UserRoleDTO getRole() {
        return role;
    }

    public void setRole(UserRoleDTO role) {
        this.role = role;
    }

    public UserStatusDTO getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(UserStatusDTO userStatus) {
        this.userStatus = userStatus;
    }

    public Set<UserAddressDTO> getAddresses() {
        return addresses;
    }

    public void setAddresses(Set<UserAddressDTO> addresses) {
        this.addresses = addresses;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Set<UserPhoneDTO> getPhones() {
        return phones;
    }

    public void setPhones(Set<UserPhoneDTO> phones) {
        this.phones = phones;
    }
}
